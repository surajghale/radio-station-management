from django.db.models import Q
from django.shortcuts import render,redirect
from radio_app.forms import ScheduleForm
from radio_app.models import Schedule
from radio_app.models import Account
from radio_app.forms import RegistrationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from radio_app.docorators import station_login_required 
from radio_app.forms import AuthenticationForm
import simplejson as json
# Create your views here.
def home(request):
    return render(request,'index.html')

def register(request):
    return render(request,'signup.html')
def login_view(request):
    # user = request.user
    context={}
    # if user.is_authenticated:
    #     return redirect('home')
    if request.method=='POST':
        form = AuthenticationForm(request.POST)
        print(form)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(email=email,password=password)
            if user:
                login(request,user)
                return redirect('home')
        else:
            print('errror')
    form = AuthenticationForm()
    context['form']=form
    return render(request,'radio_app/authenticate.html',context)
def stationsignup(request):
	context = {}
	if request.POST:
            form = RegistrationForm(request.POST)
            if form.is_valid():
                
                form.save()
                email = form.cleaned_data.get('email')
                raw_password = form.cleaned_data.get('password1')
                account = authenticate(email=email, password=raw_password)
                login(request, account)
                return redirect('/login')
            else:
                context['registration_form'] = form

	else:
		form = RegistrationForm()
		context['registration_form'] = form
	return render(request, 'radio_app/station_signup.html', context)


# def stationsignup(request):
    
#     return render(request,'radio_app/station_signup.html')
def schedulesview(request):
        # context = {}
    if request.POST:
        form = ScheduleForm(request.POST,request.FILES)
        print(form)
        if form.is_valid():
            id = form.cleaned_data['rj']
            img = form.cleaned_data['image']
            owns = form.cleaned_data['owner']
            title = form.cleaned_data['title']
            date = form.cleaned_data['datetime_field']
            print(id)
            print(title,date,img)
            form.save()
            # acc = Account.objects.filter(schedule__id=6)
    # taking value form schedule from which is connected to two table account and profile and using that value to update the account column value

            for i in id:
                print(i)
                i.is_rj=True
                i.rj_id=str(i)
                i.owner_id = str(owns)
                i.save()
            # acc = Account.objects.filter(schedule__id=1).first()
            # acc.is_rj=True
            # acc.save()
            # acc = Account.objects.filter(schedule__id=1)
            return redirect('about')
        
    else:
        form = ScheduleForm()
        print(form)
        # context['registration_form'] = form
    
    return render(request, 'radio_app/article.html', {'form':form})
def aboutview(request):
    return render(request,'radio_app/about.html')
@login_required
@station_login_required
def schedule_edit(request):
    
    user_id = request.user.id
    acc = Account.objects.get(id = user_id)
    print(str(acc))
    # she=Schedule.objects.
    owner = acc.rj_id
    sch_owner=acc.owner_id
    
    if owner==sch_owner:
        schedule =Schedule.objects.filter(Q(rj=owner) | Q(owner_id=sch_owner))
    else:
        schedule =Schedule.objects.filter(rj=owner)
    print('this schedule is {}'.format(schedule))
    context = {'schedules':schedule}
    print('the schedule is is{}'.format(schedule[0].id))
    return render(request,'radio_app/about.html',context)
@login_required
@station_login_required
def update_schedule(request,id):
    context={}
    id = str(id)
    schedule = get_object_or_404(Schedule, id = id)
    print(schedule)
    if request.method=='POST':
        form = ScheduleForm(request.POST,request.FILES,instance=schedule)
        print(form)
        if form.is_valid():
            title = form.cleaned_data['title']
            rj = form.cleaned_data['datetime_field']
            image = form.cleaned_data['image']

            print(title,rj,image)
            
            obj = form.save()
            
            schedule=obj
            context['success_message']="successfully updated"
        else:
            print("not valid")
    else:
        print("error")
        form = ScheduleForm(
			initial={
					"title": schedule.title, 
                    "image":schedule.image,
                    "date":schedule.datetime_field,
					"owner": schedule.owner,
                    "rj":   Account.objects.all(),
				}
			)
        print(form)
        # response[datetime_field] = response[datetime_field].strftime("%Y-%m-%d %H:%M:%S")
        context['form'] = form
        context['data'] = json.dumps(str(schedule.datetime_field))
    # context['form'] = schedule
    # context['acc'] = Account.objects.all()
    return render(request,'radio_app/edit_schedule.html',context)

    
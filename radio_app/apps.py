from django.apps import AppConfig


class RadioAppConfig(AppConfig):
    name = 'radio_app'
    def ready(self):
        import radio_app.signals

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.conf import settings

# Create your models here.
class MyAccountManager(BaseUserManager):
	def create_user(self, email, username, password=None):
		if not email:
			raise ValueError('Users must have an email address')
		if not username:
			raise ValueError('Users must have a username')
		user = self.model(
			email=self.normalize_email(email),
			username=username,
		)

		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, username, password):
		user = self.create_user(
			email=self.normalize_email(email),
			password=password,
			username=username,
		)
		user.is_admin = True
		user.is_staff = True
		user.is_superuser = True
		user.save(using=self._db)
		return user


class Account(AbstractBaseUser):
	email 					= models.EmailField(verbose_name="email", max_length=60, unique=True)
	username 				= models.CharField(max_length=30, unique=True)
	date_joined				= models.DateTimeField(verbose_name='date joined', auto_now_add=True)
	last_login				= models.DateTimeField(verbose_name='last login', auto_now=True)
	is_admin				= models.BooleanField(default=False)
	is_active				= models.BooleanField(default=True)
	is_staff				= models.BooleanField(default=False)
	is_superuser			= models.BooleanField(default=False)
	is_station              = models.CharField(max_length=255)
	is_normal               = models.BooleanField(default=False)
	is_rj                   = models.BooleanField(default=False)
	owner_id                = models.CharField(max_length=30,null=True,blank=True)
	image                   = models.ImageField(default='jpt.png',upload_to='images/',null=True,blank=True)
	livelink                = models.CharField(max_length=100)
	rj_id                   = models.CharField(max_length=255,null=True,blank=True)

	
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username']

	objects = MyAccountManager()

	def __str__(self):
		return str(self.id)

	# For checking permissions. to keep it simple all admin have ALL permissons
	def has_perm(self, perm, obj=None):
		return self.is_admin

	# Does this user have permission to view this app? (ALWAYS YES FOR SIMPLICITY)
	def has_module_perms(self, app_label):
		return True


class Profile(models.Model):
	user = models.OneToOneField(Account,on_delete=models.CASCADE)
	image = models.ImageField(default='default.png',upload_to='images/')
	# /livelink=models.
	counter = models.CharField(max_length=255,blank= True, null=True)
	livelink=models.CharField(max_length=255)
	trending = models.CharField(max_length=255,default=0)
	def __str__(self):
		return str(self.user.id)
# class Rjprofile(models.Model):
# 	title = models.CharField(max_length=100)
# 	# image = models.ImageField(default='jpt.png',upload_to='images/',null=True,blank=True)
# 	datetime_field_start = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
# 	datetime_field_end = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
# 	# timefield = models.TimeField(verbose_name='date joined',auto_now_add=False)
# 	owner = models.ForeignKey(Profile,on_delete=models.CASCADE)
# 	rj = models.ManyToManyField(Account,blank= True, null=True)
# 	def __str__(self):
# 		return str(self.rj)

class Schedule(models.Model):
	title = models.CharField(max_length=100)
	# image = models.ImageField(default='jpt.png',upload_to='images/',null=True,blank=True)
	datetime_field_start = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
	datetime_field_end = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
	# timefield = models.TimeField(verbose_name='date joined',auto_now_add=False)
	owner = models.ForeignKey(Profile,on_delete=models.CASCADE)
	rj = models.ManyToManyField(Account,blank= True, null=True)
	requested = models.BooleanField(default=False)
	# image = models.ManyToManyField(Image)
	def __str__(self):
		return str(self.rj)
class Rjrequest(models.Model):
	title=models.CharField(max_length=255)
	datetime_field_start = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
	datetime_field_end = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
	owner = models.ForeignKey(Profile,on_delete=models.CASCADE)
	image = models.ImageField(default='jpt.png',upload_to='images/',null=True,blank=True)
	rj = models.ForeignKey(Account,on_delete=models.CASCADE)
	
class IpAddress(models.Model):
	ip= models.CharField(max_length=255)
	location = models.CharField(max_length=255)
	listen_date = models.DateTimeField(auto_now_add=False,blank=True,null=True)
	station = models.ForeignKey(Profile,on_delete=models.CASCADE)
class UserTrack(models.Model):
	start_datefield =  models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
	end_datefield = models.DateTimeField(verbose_name='date joined',auto_now_add=False,blank=True,null=True)
class Images(models.Model):
		rjs = models.ForeignKey(Account,null=True,blank=True,on_delete=models.CASCADE)
		schedule = models.ForeignKey(Schedule, default=None,on_delete=models.CASCADE)
		image = models.ImageField(default='jpt.png',upload_to='images/',null=True,blank=True)
	
from django.db import models 
class Subscribe(models.Model):
    profile = models.ForeignKey(Profile,on_delete=models.CASCADE)
    account = models.ForeignKey(Account,on_delete=models.CASCADE)
    
class Pizza(models.Model):

    name = models.CharField(max_length=30)
    toppings = models.ManyToManyField('Topping')

    def __str__(self):
        return self.name


class Topping(models.Model):

    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Comment(models.Model):
	profile = models.ForeignKey(Profile,on_delete=models.CASCADE)
	account = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
	content = models.TextField(max_length=160)
	schedule_id = models.IntegerField(null=True,blank=True)
	reply = models.ForeignKey('Comment',null=True,related_name="replies",on_delete=models.CASCADE)
	timestamp = models.DateTimeField(auto_now_add=True)
	def  __str__(self):
		return '{}'.format(str(self.account.username))
	    

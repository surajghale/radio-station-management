from radio_app.models import Schedule
from django import forms
from django.contrib.auth.forms import UserCreationForm
from radio_app.models import Account
from django.contrib.auth import login, authenticate, logout
from tempus_dominus.widgets import DatePicker, TimePicker, DateTimePicker

class ScheduleForm(forms.ModelForm):
    datetime_field = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'useCurrent': True,
                'collapse': False,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),
    )
    class Meta:
        model = Schedule
        fields = ['title','datetime_field','owner','rj']

class RegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Add a valid email address.')
    class Meta:
        model = Account
        fields = ('email', 'username', 'password1', 'password2')
    def	save(self):
            account = Account(
                        email=self.cleaned_data['email'],
                        username=self.cleaned_data['username']
                    )
            password = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password != password2:
                return 'did not matched'
            account.set_password(password)
            account.is_station=True
            
            account.save()
            return account
class AuthenticationForm(forms.ModelForm):
    class Meta:
        model = Account
        fields=['email','password']
    def clean(self):
	    if self.is_valid():
		    email = self.cleaned_data['email']
		    password = self.cleaned_data['password']
		    if not authenticate(email=email, password=password):
			    raise forms.ValidationError("Invalid login")

    
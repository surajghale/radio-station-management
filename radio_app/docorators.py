from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
def station_login_required(function=None,
                         redirect_field_name=REDIRECT_FIELD_NAME,
                         login_url=None):
    '''
    Decorator to check that the user accessing the decorated view has their
    is_station flag set to True.
    It will first redirect to login_url or the default login url if the user is
    not authenticated. If the user is authenticated but is not staff, then
    a PermissionDenied exception will be raised.
    '''

    
    actual_decorator = user_passes_test(
        lambda u: u.is_station and u.is_rj,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

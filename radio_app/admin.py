# from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from radio_app.models import Account
from radio_app.models import Profile
from radio_app.models import Schedule,Images,Subscribe,IpAddress,Rjrequest
class AccountAdmin(UserAdmin):
	list_display = ('email','username','date_joined', 'last_login', 'is_admin','is_staff','is_station','is_normal')
	search_fields = ('email','username',)
	readonly_fields=('date_joined', 'last_login')

	filter_horizontal = ()
	list_filter = ()
	fieldsets = ()


admin.site.register(Account, AccountAdmin)
admin.site.register(Profile)
admin.site.register(Schedule)

admin.site.register(Images)
admin.site.register(Subscribe)
admin.site.register(IpAddress)
admin.site.register(Rjrequest)





from django.urls import path
from radio_app.api.views import registeration_view
from radio_app.api.views import scheduleview
from radio_app.api.views import login_view,subscribed_user_check
from radio_app.api.views import profileview,update_rj_request,show_rj_request,single_request,approve_rj,delete_rj_request,delete_subscribe
from rest_framework_jwt.views import obtain_jwt_token
from radio_app.api.views import current_user,server_view,all_schedule,single_schedule,update_rj
from radio_app.api.views import all_users,show_schedule,update_schedule,ip_track,delete_schedule,delete_schedule_account
from radio_app.api.views import user_track,add_comment,show_comment,show_image,add_subscribed_user,profile_list_view,profile_update

# from radio_app.api.views import Registration_view
# from radio_app.api.serializer import UserSerializer
app_name='radio_app'

urlpatterns=[
    path('token-auth', obtain_jwt_token),
    path('current_user/<int:id>', current_user),
    path('users',all_users,name='users'),
    path('register',registeration_view,name='register'),
    path('view_schedule',scheduleview,name ='schedule'),
    path('show_schedule/<int:id>',show_schedule,name ='show'),
    path('update_schedule/<int:id>',update_schedule,name ='update'),
    path('login',login_view,name='login'),
    path('profile/<int:id>/',profileview,name='profile'),
    path('userTrack',user_track,name="usertrack"),
    path('comments/<int:id>',add_comment,name="comment"),
    path('image/',show_image,name="image"),
    path('show_comment/<int:id>',show_comment,name="listcomment"),
    path('subscribed_user/',add_subscribed_user,name="subscription"),
    path('profile_list/',profile_list_view,name="profile_list"),
    path('ip_track/',ip_track,name="track_ip"),
    path('profile_update/<int:id>',profile_update,name='profile_update'),
    path('delete_schedule/<int:id>',delete_schedule,name='delete'),
    path('delete_schedule_account/<int:id>',delete_schedule_account,name='delete_schedule'),
    path('server',server_view,name='server'),
    path('all_schedule',all_schedule,name='all'),
    path('single_schedule/<int:id>',single_schedule,name='single'),
    path('update_rj/<int:id>',update_rj,name='rj'),
    path('update_rj_request/<int:id>',update_rj_request,name='rj_request'),
    path("show_rj_request",show_rj_request,name="request_list"),
    path('single_request/<int:id>',single_request,name='single_request'),
    path('approve_rj/<int:id>',approve_rj,name='approve'),
    path('delete_rj_request/<int:id>',delete_rj_request,name='request_delete'),
    path('sub_check/<int:id>/<int:id1>',subscribed_user_check,name='sub'),
    path('delete_subscribe/<int:id>/<int:id1>',delete_subscribe,name='del_sub'),





]
from rest_framework import serializers
from radio_app.models import Account
from radio_app.models import Schedule
from radio_app.models import Profile
from django.contrib.auth import login, authenticate, logout
from rest_framework_jwt.settings import api_settings
from radio_app.models import UserTrack
from radio_app.models import Comment, Images, Subscribe, IpAddress, Rjrequest

class RegistrationSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()
    password2 = serializers.CharField(
        style={'input_type': 'password'}, write_only=True)
    # is_station = serializers.CharField()

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    # def create(self, validated_data):
    #     password = validated_data.pop('password', None)
    #     instance = self.Meta.model(**validated_data)
    #     if password is not None:
    #         instance.set_password(password)
    #     instance.save()
    #     return instance

    def save(self):
        account = Account(
            email=self.validated_data['email'],
            username=self.validated_data['username'],
            image=self.validated_data['image'],
            livelink=self.validated_data['livelink'],
            #    / is_station = self.validated_data['is_station']

        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']
        is_station = self.validated_data['is_station']
        print("station is {}".format(is_station))
        if password != password2:
            raise serializers.ValidationError(
                {'password': 'Passwords must match.'})
        account.set_password(password)
        # account.is_station=True

        if is_station == "station":
            account.is_station = True
        else:
            account.is_station = False
        account.save()
        return account

    class Meta:
        model = Account
        fields = ['token', 'is_station', 'email', 'username',
                  'image', 'livelink', 'password', 'password2']
        extra_kwargs = {
            'password': {'write_only': True},
        }


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['id', 'title', 'datetime_field_start',
                  'datetime_field_end', 'rj', 'owner', 'requested']


class ScheduleSerializer1(serializers.ModelSerializer):
    # rj = serializers.ListField()
    # image = serializers.CharField()
    class Meta:
        model = Schedule
        fields = ['id', 'title', 'datetime_field_start',
                  'datetime_field_end', 'rj', 'owner', 'requested']


class RjrequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rjrequest
        fields = ['id', 'title', 'image', 'datetime_field_start',
                  'datetime_field_end', 'rj', 'owner']


class ImageSerializer(serializers.ModelSerializer):
    image = serializers.ListField()

    class Meta:
        model = Images
        fields = ['id', 'rjs', 'schedule', 'image']


class ImageSerializer1(serializers.ModelSerializer):
    image = serializers.CharField()

    class Meta:
        model = Images
        fields = ['id', 'rjs', 'schedule', 'image']


class ImageSerializerDetail(serializers.ModelSerializer):
    # image = serializers.ListField()
    class Meta:
        model = Images
        fields = ['id', 'rjs', 'schedule', 'image']


class IpSerializer(serializers.ModelSerializer):
    class Meta:
        model = IpAddress
        fields = ['ip', 'location', 'listen_date', 'station']


class SubscribeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscribe
        fields = ['profile', 'account']
 

class SubscribeSerializercheck(serializers.ModelSerializer):
    class Meta:
        model = Subscribe
        fields = ['profile', 'account']


class UserTrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTrack
        fields = ['end_datefield', 'start_datefield']


class AuthenticationForm(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def clean(self):
        if self.is_valid():
            # data ={}
            email = self.validated_data['email']
            password = self.validated_data['password']
            if not authenticate(email=email, password=password):
                raise forms.ValidationError("Invalid login")


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['id', 'image', 'user_id', 'livelink', 'counter']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'is_station', 'is_rj']


class CommentSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id', 'content', 'reply', 'schedule_id']

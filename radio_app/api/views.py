from rest_framework import status
from django.db.models import Q
import base64
from django.conf import settings

from rest_framework.response import Response
from django.shortcuts import render, redirect
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from radio_app.api.serializer import RegistrationSerializer, ScheduleSerializer1, ImageSerializer1
from radio_app.api.serializer import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from radio_app.api.serializer import ProfileSerializer
from radio_app.signals import create_profile
from radio_app.models import Account
from radio_app.models import Profile
from radio_app.api.serializer import ScheduleSerializer, RjrequestSerializer, SubscribeSerializercheck
from radio_app.forms import ScheduleForm
from django.http import HttpResponse, JsonResponse
from radio_app.api.serializer import UserSerializer, ImageSerializer, IpSerializer, ImageSerializerDetail
from rest_framework import permissions, status
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from knox.models import AuthToken
from radio_app.models import Schedule, Comment, Images, Subscribe, Rjrequest
from radio_app.api.serializer import UserTrackSerializer, CommentSerialzer, SubscribeSerializer
from django.forms import modelformset_factory
from io import StringIO
# import schedule
import time
import datetime
# from django.core.files.uploadedfile import InMemoryUploadedFile


@api_view(['POST'])
def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.data)
        data = {}
        print(form)
        if form.is_valid():
            email = form.validated_data['email']
            password = form.validated_data['password']

            # print(email,password)
            user = authenticate(email=email, password=password)
            print("user is {}".format(user))
            if user:
                login(request, user)
                station = Account.objects.get(id=request.user.id)
                # print(user.username)
                data['email'] = email
                data['message'] = "successfully login"
                data['username'] = request.user.username
                data['id'] = request.user.id
                data['is_station'] = station.is_station
                data['is_rj'] = station.is_rj

                # data['email']=account.email
        else:
            # print(form.errors)
            print("errors")
        return Response(data)


@api_view(['POST', ])
def registeration_view(request):
    permission_classes = (permissions.AllowAny,)
    # authentication_classes = (TokenAuthentication,)
    print('jpt')
    if request.method == 'POST':
        seria = RegistrationSerializer(data=request.data)
        data = {}
        print(seria)
        if seria.is_valid():
            station = seria.validated_data['is_station']
            print("nepal is {}".format(station))
            account = seria.save()
            # data['message']="successfully registered"
            # data['email'] = account.email
            data['username'] = RegistrationSerializer(
                account, context={'request': request}).data,
    
        else:
            # data = seria.errors
            data['username'] = ["email already exists"]
            print("errors is ++++++++++++", seria.errors)
        return Response(data)
    else:
        print('failed')

@api_view(['POST', ])
def user_track(request):

    if request.method == 'POST':
        track = UserTrackSerializer(data=request.data)
        data = {}
        print(track)
        if track.is_valid():

            track.save()
            data['date'] = track.data['end_datefield']
            data['message'] = "success"
        return Response(data)


@api_view(['POST', ])
def ip_track(request):

    if request.method == 'POST':
        tracks = IpSerializer(data=request.data)
        data = {}
        print(tracks)
        if tracks.is_valid():
            station_id = tracks.validated_data['station']
            profile_id = station_id.id
            print("station id is {}", station_id)
            profile = Profile.objects.get(id=profile_id)
            profile_trending = int(profile.trending)+1
            profile.trending = profile_trending
            profile.save()
            tracks.save()
            # data['date'] = track.data['ip']
            data['message'] = "success"
        else:
            print("the error is".format(tracks.errors, status=400))
        return Response(data)


@api_view(['POST', ])
def add_comment(request, id):
    profile = Profile.objects.get(id=id)
    # current_user = request.user
    # print("current user is {}".format(current_user))
    account = Account.objects.get(id=1)
    if request.method == 'POST':
        data = {}
        serialzer = CommentSerialzer(data=request.data)
        reply_id = request.POST.get('id')
        content = request.POST.get('content')
        schedule_id = request.POST.get('schedule_id')
        print("reply id is {} {}".format(reply_id, content))
        print(serialzer)
        if serialzer.is_valid():
            content = serialzer.validated_data['content']
            # reply_id = serialzer.validated_data['id']
            # print("reply is {}{}".format(reply_id,content))
            comment_qs = None
            if reply_id:
                comment_qs = Comment.objects.get(id=reply_id)
            comments = Comment.objects.create(
                profile=profile, account=account, reply=comment_qs, content=content, schedule_id=schedule_id)
            comments.save()
            data['message'] = "success"
        else:
            print('error')
        return Response(data)


@api_view(['POST', ])
def add_subscribed_user(request):
    
    if request.method == 'POST':
        data = {}
        seria = SubscribeSerializer(data=request.data)
        profile = request.POST.get('profile')
        # account = request.POST.get('account')

        # print("profile and account id are {} {}".format(profile,account))
        print(seria)
        if seria.is_valid():
            # profile = seria.validated_data['profile']
            account = seria.validated_data['account']
            # match = Subscribe.objects.get(account=str(account))
            # print("the match is {}".format(match))
            print("profile is {}", profile)

            def clean():
                try:
                    match = Subscribe.objects.get(
                        account=str(account), profile=str(profile))
                    return 'match'
                except Exception:
                    return 'failed'
            suc = clean()

            print("succ is {} {}".format(suc, profile))
            if suc == "match":
                print("the account already exists")
            else:
                station_account = Profile.objects.get(user_id=str(profile))
                count = station_account.counter
                seria.save()
                print("the if count is {}".format(count))
                if count == None:
                    count = 1
                    # print("the if count is{}".format(count))
                    station_account.counter = str(1)
                    station_account.save()
                else:
                    count = int(count)+1
                    print("the ele count is{}".format(count))
                    station_account.counter = str(count)
                    station_account.save()
                print("profile and account id are {} {}".format(profile, account))
            profile_id = Profile.objects.get(user_id=profile)
            number_subscriber = profile_id.counter
            print("number of subscriber are", number_subscriber)
            data['num_subscriber'] = number_subscriber
            
            data['message'] = "success"
        else:
            print('error')
        return Response(data)

# for checking whether the logged in user has signed in or not==================


@api_view(['GET', ])
def subscribed_user_check(request, id, id1):
    # print("logged in user is ======={}",request.account.id)
    if request.method == 'GET':
        data = {}

        def check():
            try:
                profile = Subscribe.objects.filter(profile=id)
                # prof = profile.profile
                # account = profile.account
                print("profile and account are {}".format(profile))
                match = Subscribe.objects.get(
                    account=str(id1), profile=str(id))
                return match

                # for i in profile:

                #     print("individual are {}",type(int(i.account)))
                #     if 1==1:
                #         return "match"
                #     match = Subscribe.objects.get(account=str(id1),profile=str(id))
                #     print("mathedd ----------------------")
                #

            except Exception:
                return 'failed'
        sub = check()
        print("sub is ==========", sub)
        if(sub != "failed"):
            serialzer = SubscribeSerializercheck(sub)

            data['message'] = "subscribed"
        else:
            data["message"] = "unsubscribed"

       
        return Response(data)


@api_view(['POST', ])
def scheduleview(request):
    if request.method == 'POST':
        sche = ScheduleSerializer(data=request.data, many=True)
        data = {}
        
        img = ImageSerializer(data=request.data, many=True)
        # print('image is {}'.format(img))

        i = img.initial_data['image']

        print("i is {} {} ".format(sche, img))
        
        if sche.is_valid() and img.is_valid():
            i = img.data
            print('serialze is {}'.format(i))

            # email=sche.validated_data['email']
            # print(email)
            owner = sche.initial_data['owner']
            rj = sche.initial_data['rj']
            datetime_field_start = sche.initial_data['datetime_field_start']
            datetime_field_end = sche.initial_data['datetime_field_end']
            title = sche.initial_data['title']
            image_list = img.initial_data.getlist('image')

            
            lis = list(rj.split(','))
            # print('image list is {}'.format(lis_img))

            profile = Profile.objects.get(id=owner)
            # id=sche.validated_data['rj']
            print("id is {} {} {} {}".format(owner, rj, title, lis))
            # for i in lis:
            # print("image is {}".format(i))
            # print("image is {}".format(type(i)))
            schedule = Schedule.objects.create(
                title=title, datetime_field_start=datetime_field_start, datetime_field_end=datetime_field_end, owner=profile)
            schedule.save()
           
            li = []
            # # schedule = sche.save(rj=[1,2])
            for i in lis:
                ind = Account.objects.get(id=i)
                li.append(ind)
            
            last = Schedule.objects.last()
            print('last is {}'.format(last.id))
            lastSchedule = last.id
            schedule2 = Schedule.objects.get(id=lastSchedule)
            schedule2.rj.add(*li)
            
            for i in range(0, len(image_list)):
                print("image {}".format(image_list[i]))
                print("image  {}".format(type(image_list[i])))
                photo = Images.objects.create(
                    schedule=schedule, image=image_list[i], rjs=li[i])
                photo.save

                # print("photo is {}".format(photo))
            # photo.save()
            print("rj is ==========={}".format(li))
            for n in li:
                print("success========")
                # print("account is {}".format(account))
                n.is_rj = True
                n.rj_id = str(n)
                n.owner_id = str(owner)
                n.save()

            # print(schedule)
            # for i in
            data['message'] = "success"
            data['title'] = schedule.title
            data['date'] = schedule.datetime_field_start
        else:
            # data = sche.errors
            print('errors')
        return Response(data)
# REQUEST FOR RJ UPDATE


@api_view(['POST', ])
def update_rj_request(request, id):
    # schedule = Schedule.objects.get(rj = id)
    # print(schedule)
    # img = Images.objects.get(rj=id)
    schedule = Schedule.objects.get(rj=id)
    schedule_id = schedule.id
    rj_schedule = Schedule.objects.get(id=schedule_id)
    if request.method == 'POST':
        data = {}
        sche = RjrequestSerializer(data=request.data)
        print("schedule is ", sche)
        if sche.is_valid():
            rj_schedule.requested = True
            rj_schedule.save()
            # owner = sche.validated_data['owner']
            sche.save()
            data['success_message'] = "successfully updated"
            data['updated_rj'] = sche.data
            # data['image_update'] = image.data
        return Response(data)
# SHOW ALL SCHEDULE LIST


@api_view(['GET', ])
def all_schedule(request):
    
    datas = Schedule.objects.all()
    print(datas)
    data = {}
    if request.method == 'GET':
        # datas = Profile.object.filter(user_id=id)

        pro = ScheduleSerializer(datas, many=True)
        print(pro)
        # pro_img = str(pro.data['image'])
        # correct = pro_img.replace('/front_react/public/','')
        # print(correct)
        data['message'] = 'success'

        # data['id']=pro.data['user_id']
        data['schedule'] = pro.data
        # print(pro)
        # print(pro.data['image'])
        return Response(data)


@api_view(['GET', ])
# @permission_classes((IsAuthenticated,))
def show_schedule(request, id):

    # return Response(user_id.data)
    acc = Account.objects.get(id=id)
    print("acccount is {}".format(acc))
    # she=Schedule.objects.
    owner = acc.rj_id
    print("owner is {} ".format(owner))
    sch_owner = acc.owner_id
    data = {}
    # datas = Schedule.objects.filter(id=owner)
    if owner == sch_owner:
        schedule = Schedule.objects.filter(owner_id=sch_owner)
        print("frist schedule {}".format(schedule))
        # schedule_list = ScheduleSerializer(schedule,many=True)
        # data['schedule']= schedule_list.data
    else:
        schedule = Schedule.objects.filter(rj=owner)
        print("second schedule {}".format(schedule))
        # schedule =Schedule.objects.filter(Q(rj=owner) | Q(owner_id=sch_owner))
        # schedule_list = ScheduleSerializer(schedule,many=True)
        # data['schedule']= schedule_list.data
    print(schedule)
    if request.method == "GET":
        schedule_list = ScheduleSerializer(schedule, many=True)
        print("jpt is {}".format(schedule_list))
        data['message'] = "sucess comment"
        print(schedule_list)
        data['schedule'] = schedule_list.data
        return Response(data)


@api_view(['GET', ])
def show_rj_request(request):
    req = Rjrequest.objects.all()
    data = {}
    if request.method == "GET":
        req_rj = RjrequestSerializer(req, many=True)
        data['message'] = "success"
        data['rj_update_request'] = req_rj.data
        return Response(data)


@api_view(['GET', ])
def show_image(request):
    
    datas = Images.objects.all()
    print(datas)
    data = {}
    if request.method == 'GET':
        # datas = Profile.object.filter(user_id=id)

        pro = ImageSerializerDetail(datas, many=True)
        print(pro)
        # pro_img = str(pro.data['image'])
        # correct = pro_img.replace('/front_react/public/','')
        # print(correct)
        data['message'] = 'success'

        # data['id']=pro.data['user_id']
        data['Rj_Detail'] = pro.data
        # print(pro)
        # print(pro.data['image'])
        return Response(data)

# IT IS USED TO CHANGE THE RJJ ID ,OWNER ID AND IS RJ TO FALSE WHEN SCHEDULE IS DELETED


@api_view(['POST', ])
def delete_schedule_account(request, id):
    schedule = Schedule.objects.get(id=id)
    sche = ScheduleSerializer(schedule)
    # image = Images.objects.get(id=id)
    schedule_list = ScheduleSerializer(schedule)
    rj = schedule_list.data
    rjs = rj['rj']
    data = {}
    print("schedule is {}".format(rjs))
    # if request.method=='POST':
    for rj in rjs:
        account = Account.objects.get(id=rj)
        account.is_rj = False
        account.rj_id = None
        account.owner_id = None
        account.save()
        data['messate'] = "sucess"
        print("account is {}".format(account.is_rj))
    return Response(data)


@api_view(['DELETE'])
def delete_schedule(request, id):
    schedule = Schedule.objects.get(id=id)
    image = Images.objects.filter(schedule=id)
    data = {}
    if request.method == 'DELETE':
        schedule.delete()
        image.delete()
        data['message'] = "deleted successfully"
        return Response(data)


@api_view(['DELETE'])
def delete_subscribe(request, id, id1):
    sub = Subscribe.objects.get(profile=str(id), account=str(id1))
    data = {}
    if request.method == 'DELETE':
        sub.delete()
        profile_update = Profile.objects.get(user=id)
        print("before==========", profile_update.counter)
        counter = int(profile_update.counter)-1
        print("after ============", counter)
        profile_update.counter = counter
        profile_update.save()
        data['message'] = "deleted successfully"
        data['counter'] = counter
        return Response(data)


@api_view(['DELETE'])
def delete_rj_request(request, id):
    rj_request = Rjrequest.objects.get(rj=id)
    schedule = Schedule.objects.get(rj=id)
    schedule.requested = False
    schedule.save()

    # image = Images.objects.filter(schedule=id)
    data = {}
    if request.method == 'DELETE':
        rj_request.delete()

        data['message'] = "deleted successfully"
        return Response(data)


@api_view(['POST'])
def server_view(request):

    data = {}

    def job():
        for i in [1, 2, 3]:
            profile = Profile.objects.get(id=i)
            profile.trending = 0
            profile.save()
            print('success')
    schedule.every().day.at("09:39").do(job)
    # schedule.every().day.at("09:20").do(job)

    print(datetime.datetime.now().strftime('%A'))

    def jpt():
        while True:
            schedule.run_pending()
            time.sleep(1)
    jpt()
    return Response(data)


@api_view(['GET', ])
def show_comment(request, id):
    
    data = {}
    # datas = Schedule.objects.filter(id=owner)
    # if owner==sch_owner:
    schedule = Comment.objects.filter(profile_id=id)
    
    print(schedule)
    if request.method == "GET":
        schedule_list = CommentSerialzer(schedule, many=True)
        print(schedule_list)
        data['schedule'] = schedule_list.data
        return Response(data)


@api_view(['PUT', ])
def update_schedule(request, id):
    # context={}
    # id = str(id)
    schedule = Schedule.objects.get(id=id)
    print(schedule)
    img = Images.objects.filter(schedule=id).order_by('id')
    if request.method == 'PUT':
        data = {}
        sche = ScheduleSerializer(schedule, data=request.data)
        image = ImageSerializer(img, data=request.data)
        print("image is ==========={}".format(img[0].image))
        img_list = image.initial_data.getlist('image')
        print("image lst is is =========={}".format(img_list))
        # sche = ScheduleSerializer(data=request.data)
        # data ={}
        item = sche.initial_data['rj']
        print(sche)
        items = sche.initial_data.pop('rj')
        item = items[0]
        print("item is {}".format(item))
        print(sche, items)
        lis = list(item.split(','))
        print('list is {}'.format(lis))
        s1 = list(Schedule.objects.all())
        print('type of schedule is {}'.format(type(s1)))
        if sche.is_valid() and image.is_valid():
            owner = sche.validated_data['owner']
            # print("validated title is ========{}".format(owner))
            # image.save()
            sche.save()
            li = []
            # schedule = sche.save(rj=[1,2])
            for i in lis:
                ind = Account.objects.get(id=i)
                li.append(ind)
            # sh=Schedule.objects.get(id=50)
            # print(sh)
            # sh.rj.add(*li)
            print('query set is {}'.format(li))
            
            schedule2 = Schedule.objects.get(id=id)
            schedule2.rj.add(*li)
            # sche_update = Schedule.objects.filter(id=)
            for i in range(0, len(img_list)):
                img0 = img[i]
                img0.image = img_list[i]
                img0.rj = li[i]
                img0.save()

            for n in li:
                # print("account is {}".format(account))
                n.is_rj = True
                n.rj_id = str(n)
                n.owner_id = str(owner)
                n.save()
            # title = form.cleaned_data['title']
            # rj = form.cleaned_data['datetime_field']
            # image = form.cleaned_data['image']

            # print(title,rj,image)
            # obj = form.save()
            # schedule=obj
            data['success_message'] = "successfully updated"
            data['updated_schedule'] = sche.data
        return Response(data)
    
#  UPDATE RJ SCHEDULE================


@api_view(['PUT', ])
def update_rj(request, id):
    schedules = Schedule.objects.get(rj=id)
    sche1 = schedules.id
    schedule = Schedule.objects.get(id=sche1)
    print(schedule)
    img = Images.objects.get(rjs=id)
    

    if request.method == 'PUT':
        data = {}
        sche = ScheduleSerializer(schedule, data=request.data)
        image = ImageSerializerDetail(img, data=request.data)
        # print("image is ==========={}".format(img[0].image))
        # img_list = image.initial_data.getlist('image')
        # print("image lst is is =========={}".format(img_list))
        # # sche = ScheduleSerializer(data=request.data)
        # # data ={}
        print("detaul============{} {}", sche, image)
        item = sche.initial_data['rj']
        print(sche)
        items = sche.initial_data.pop('rj')
        item = items[0]
        print("item is {}".format(item))
        print(sche, items)
        lis = list(item.split(','))
        print('list is {}'.format(lis))
        s1 = list(Schedule.objects.all())
        print('type of schedule is {}'.format(type(s1)))
        if sche.is_valid() and image.is_valid():
            # owner = sche.validated_data['owner']
            # print("validated title is ========{}".format(owner))
            # image.save()
            sche.save()
            li = []
            # schedule = sche.save(rj=[1,2])
            for i in lis:
                ind = Account.objects.get(id=i)
                li.append(ind)
            # sh=Schedule.objects.get(id=50)
            # print(sh)
            # sh.rj.add(*li)
            print('query set is {}'.format(li))
            
            schedule2 = Schedule.objects.get(id=sche1)
            schedule2.rj.add(*li)
            # sche_update = Schedule.objects.filter(id=)
            image.save()

            data['success_message'] = "successfully updated"
            data['updated_schedule'] = sche.data
            data['image_update'] = image.data
        return Response(data)


# VIEW FOR THE APPROVING THE REQEUST BY ADMIN

@api_view(['PUT', ])
def approve_rj(request, id):
    schedules = Schedule.objects.get(rj=id)
    sche1 = schedules.id
    lis = Schedule.objects.filter(id=sche1)[0].rj.all()
    li = []
    for i in lis:
        li.append(i.id)
    schedule = Schedule.objects.get(id=sche1)
    image = Images.objects.get(rjs=id)
    print("image is", image)
    print(schedule)
    if request.method == 'PUT':
        data = {}
        sche = ScheduleSerializer1(schedule, data=request.data)
        img = ImageSerializer1(image, data=request.data)
        # # item = sche.initial_data.pop('rj')
        img1 = str(img.initial_data['image'])
        print("sche and image are {} {}", sche, img, img1)
        correct = img1.replace('/my-app/public/', '')

        if sche.is_valid():
            
            print("image is ", img1)

            sche.save()
            schedule2 = Schedule.objects.get(id=sche1)
            schedule2.rj.add(*li)
            image.image = correct
            image.save()
            
            data['success_message'] = "successfully updated"
            data['updated_schedule'] = sche.data
            # data['updated_image'] = img.data
        else:
            print("failed======================={}", img.errors)
        return Response(data)


@api_view(['PUT', ])
def profile_update(request, id):
    profile = Profile.objects.get(user_id=id)
    if request.method == 'PUT':
        data = {}
        prof = ProfileSerializer(profile, data=request.data)
        if prof.is_valid():
            prof.save()
            data['message'] = "successfull"
            data['profile'] = prof.data
        else:
            print("errors")
        return Response(data)


@api_view(['GET', ])
def profileview(request, id):
    # try:
    #     datas =Profile.objects.get(user_id=id)
    # except Profile.DoesNotExist:
    #     return HttpResponse(status=404)
    # logg=request.account
    # print('logged in user is {} '.format(logg))
    datas = Profile.objects.get(user_id=id)
    print(datas)
    data = {}
    if request.method == 'GET':
        # datas = Profile.object.filter(user_id=id)

        pro = ProfileSerializer(datas)
        print(pro)
        pro_img = str(pro.data['image'])
        correct = pro_img.replace('/my-app/public/', '')
        print(correct)
        data['message'] = 'success'
        data['counter'] = pro.data['counter']
        data['image'] = correct
        data['id'] = pro.data['user_id']
        data['livelink'] = pro.data['livelink']
        print("correct is ======", correct)
        # print(pro)
        # print(pro.data['image'])
        return Response(data)
# GET SINGLE SCHEDULE FROM SCHEDULE TABLE


@api_view(['GET', ])
def single_schedule(request, id):
    datas = Schedule.objects.get(rj=id)
    datas1 = Images.objects.get(rjs=id)
    print(datas)
    data = {}
    if request.method == 'GET':
        # datas = Profile.object.filter(user_id=id)

        pro = ScheduleSerializer(datas)
        pro1 = ImageSerializerDetail(datas1)
        print(pro)
        pro_img = str(pro1.data['image'])
        correct = pro_img.replace('/my-app/public/', '')
        print(correct)
        # pro_img = str(pro.data['image'])
        # correct = pro_img.replace('/my-app/public/','')
        # print(correct)
        data['message'] = 'success'
        data['title'] = pro.data['title']
        data['start_date'] = pro.data['datetime_field_start']
        data['end_date'] = pro.data['datetime_field_end']
        data['owner'] = pro.data['owner']
        data['rj'] = pro.data['rj']
        data['image'] = correct
        data['requested'] = pro.data['requested']
        # data['livelink']=pro.data['livelink']
        # print("correct is ======",correct)
        # print(pro)
        # print(pro.data['image'])
        return Response(data)

# SINGLE REQEUST DATA ===============================


@api_view(['GET', ])
def single_request(request, id):
    datas = Rjrequest.objects.get(rj=id)
    # datas1 = Images.objects.get(rj=id)
    print(datas)
    data = {}
    if request.method == 'GET':
        # datas = Profile.object.filter(user_id=id)
        pro = RjrequestSerializer(datas)
        # pro1 = ImageSerializerDetail(datas1)
        print(pro)
        pro_img = str(pro.data['image'])
        correct = pro_img.replace('/my-app/public/', '')
        print(correct)
        # pro_img = str(pro.data['image'])
        # correct = pro_img.replace('/my-app/public/','')
        # print(correct)
        data['message'] = 'success'
        data['title'] = pro.data['title']
        data['start_date'] = pro.data['datetime_field_start']
        data['end_date'] = pro.data['datetime_field_end']
        data['owner'] = pro.data['owner']
        data['rj'] = pro.data['rj']
        data['image'] = correct
        # data['requested'] = pro.data['requested']
        # data['livelink']=pro.data['livelink']
        # print("correct is ======",correct)
        # print(pro)
        # print(pro.data['image'])
        return Response(data)
# Profile list ==================================


@api_view(['GET', ])
def profile_list_view(request):
    # try:
    #     datas =Profile.objects.get(user_id=id)
    # except Profile.DoesNotExist:
    #     return HttpResponse(status=404)
    # logg=request.account
    # print('logged in user is {} '.format(logg))
    datas = Profile.objects.all()
    print(datas)
    data = {}
    if request.method == 'GET':
        # datas = Profile.object.filter(user_id=id)

        pro = ProfileSerializer(datas, many=True)
        print(pro)
        # pro_img = str(pro.data['image'])
        # correct = pro_img.replace('/front_react/public/','')
        # print(correct)
        data['message'] = 'success'

        # data['id']=pro.data['user_id']
        data['station'] = pro.data
        # print(pro)
        # print(pro.data['image'])
        return Response(data)
# from relationship_app.models import Acc
# Create your views here.


@api_view(['GET'])
def current_user(request, id):
    """
    Determine the current user by their token, and return their data
    """
    datas = Account.objects.get(id=id)

    data = {}
    if request.method == 'GET':
        serializer = UserSerializer(datas)
        print(serializer)
        data['id'] = serializer.data['id']
        data['username'] = serializer.data['username']
        data['is_station'] = serializer.data['is_station']
        data['message'] = 'current user'
        return Response(data)


@api_view(['GET', ])
def all_users(request):
    datas = Account.objects.all()
    data = {}
    if request.method == 'GET':
        serializer = UserSerializer(datas, many=True)
        # print(serializer)
        # data['id']=serializer.data['id']
        # data['username'] = serializer.data['username']
        # data['message']='current user'
        return Response(serializer.data)

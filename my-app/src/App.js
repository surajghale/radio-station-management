// import React from 'react';
import logo from "./logo.svg";
import "./App.css";
import React, { Component } from "react";
import Main from "./components/main";
import Login from "./components/account/login";
// import Login from './components/Account/login'
// import Register from './components/Account/register'
import AuthRoute from "./components/authRoute";
// import bootstrap from "bootstrap"; // eslint-disable-line no-unused-vars
// import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
// import * as jquery3 from '../node_modules/jquery/dist/jquery.min';
// import ShowSchedule from './components/showSchedule';
// import Registration from './components/registration';
// import Header from './components/header';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Logout from "./components/logout";
import Success from "./components/success";
// import axios from 'axios';
import { connect } from "react-redux";
// import Dash from './components/main'
import ProtectedRoute from "./components/protected_route";
import Profilestation from "./components/profile/profile_page";
import AddSchedule from "./components/schedule/addSchedule";
import Register from "./components/account/register";
import ShowSchedule from "./components/schedule/show_schedule";
import ScheduleUpdate from "./components/schedule/update_schedule";
import Rj from "./components/rj/rj_list";
import RjDetail from "./components/rj/rj_detail";
import RjProfile from "./components/profile/rj_profile";
import RjRequestedList from "./components/rj/requested_rj_list";
import Station from "./components/front/station";
import StationDetail from "./components/front/detail_station";
import ShowStation from "./components/front/show_station";
// import Home from './components/home'
// import Aux from './hoc/Aux'
// import Profilestation from './components/profile';
import * as actions from "./store/actions/actions";
// import logout from './components/logout'
// import Addschedule from './components/addSchedule'
// import ScheduleList from './components/showSchedule';
import stationList from "./components/front/station_list";
import showList from "./components/front/show_station_1";
import AddRj from "./components/rj/add_rj";
// import ProfileUpdate from './components/profile_update';
// import ProtectedRoute from './components/protected_route'
// import Nepal from './components/jpt';
// import Charts from './components/chart'
// import AuthRoute from './components/authRoute'
// import UpdatedSchedule from './components/updateSchedule'
// import MainDashboard from './components/Dashboard2/mainDash'
// import Sidebar from './components/Dashboard2/SideBar'
// import Dash from './components/dashboard4/header'
// import header from './components/dashboard4/header'
// import addScheduleDash from './components/dashboard4/add_schedule'
// import Profile from './components/dashboard4/profile'
// import Rj_info from "./components/dashboard4/rj_list";
// import RjDetail from './components/dashboard4/detail_rj'
// import DashHome from './components/home'
// import Home from './components/web/container'
// import header from './components/Dashboard/header';
// var jpt = this.props.isAuthenticated
class App extends Component {
  _isMounted = false;

  state = {
    loginStatus: this.props.isAuthenticated,
    user: null,
    back: false,
  };
  // checkStatus=()=>{
  //   axios.get('http://127.0.0.1:8000/account/api/login',{withCredentials=true}).then(
  //     res=>{
  //       console.log(res)
  //     }
  //   )
  // }
  componentDidMount() {
    this._isMounted = true;
    this.props.signinCheck();
    this.props.servertimecheck();
    console.log("update  is &&&&&&&&&&&&&&: ", this.props.update);
    // console.log(new Date())
    // this.starttime()
    // $('.selectpicker').selectpicker();
    // var jpt =localStorage.getItem('token')
    // if (jpt) {
    //   axios.get(`http://127.0.0.1:8000/account/api/current_user/${this.props.loginId}`, {
    //     headers: {
    //       Authorization: `JWT ${localStorage.getItem('token')}`
    //     }
    //   }).then(res => {
    //       console.log(res.data)
    //     });
    // }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    console.log("register is ", this.props.isregistered);
    // console.log('logged in username is : ',this.props.loginId)
    // console.log('update profile : ',this.props.success)

    var id = localStorage.getItem("id");
    var station = localStorage.getItem("is_station");
    console.log("id is ", id, station);
    return (
      <div>
        <Router>
          {/* {this.props.isAuthenticated?null:<DashHome/>} */}
          {/* <Dash/> */}
          {/* <Header/>  */}
          <Switch>
            {/* {this.props.isregistered?<Route path="/success" component={Success}/>
:null} */}
            <AuthRoute path="/" exact user={id} station={station}></AuthRoute>
            <ProtectedRoute
              path="/dashboard"
              component={Main}
              user={id}
              station={station}
            />
            <Route path="/logout" component={Logout} />
            <Route path="/register" component={Register} />
            <Route path="/success" component={Success} />
            <ProtectedRoute
              path="/show_rj"
              component={Rj}
              user={id}
              station={station}
            />
            <ProtectedRoute
              path="/profile/:id"
              exact
              component={Profilestation}
              user={id}
              station={station}
            />
            <ProtectedRoute
              path="/rj_profile/:id"
              exact
              component={RjProfile}
              user={id}
              station={station}
            />

            <ProtectedRoute
              path="/detail_rj_dash/:id"
              exact
              component={RjDetail}
              user={id}
              station={station}
            />
            <ProtectedRoute
              path="/add_schedule"
              component={AddSchedule}
              user={id}
              station={station}
            />
            <ProtectedRoute
              path="/show_schedule"
              component={ShowSchedule}
              user={id}
              station={station}
            />
            <ProtectedRoute
              path="/requested_list"
              component={RjRequestedList}
              user={id}
              station={station}
            />
            <ProtectedRoute
              path="/schedule_update/:id"
              component={ScheduleUpdate}
              user={id}
              station={station}
            />

            <Route path="/add_rj" component={AddRj} />

            {/* for front-end ============================= */}
            <Route path="/station" component={Station} />
            <Route path="/station_detail/:id" component={StationDetail} />

            <Route path="/show_station" component={ShowStation} />
            <Route path="/station_list" component={stationList} />
            <Route path="/station_list_1" component={showList} />

            {/* <Route path="/dashboard" exact component={Main}/> */}
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.log.login,
    loginId: state.log.userId,
    isregistered: state.regis.registered,
    // profile:state.prof,
    isStation: state.log.station,
    // update:state.update.suc,
    // success:state.update.success,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    signinCheck: () => dispatch(actions.signInCheck()),
    servertimecheck: () => dispatch(actions.servertime()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);

import React from "react";
import { Route, Redirect } from "react-router-dom";
import Login from "../components/account/login";
const AuthRoute = ({ component: Component, user, station, props, ...rest }) => {
  if (user) {
    return <Redirect to="/dashboard" />;
  } else {
    return <Route path="/" component={Login} />;
  }
};

export default AuthRoute;

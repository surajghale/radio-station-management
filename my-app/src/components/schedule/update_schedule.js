import React, { Component } from "react";
import { connect } from "react-redux";
import Header from "../header";
import SideBar from "../sidebar";
import DatePicker from "react-datepicker";
import Select from "react-select";
import * as actions from "../../store/actions/actions";
// import React from 'react';
import Aux from "../../hoc/Aux";
import axios from "axios";
class AddSchedule extends Component {
  state = {
    title: null,
    image: [],
    name: [],
    name_list: [],
    owner: null,
    start_time: null,
    end_time: null,
    rj: [],
    mul: [],
    update_schedule_id: null,
  };
  _isMounted = false;
  jpt = () => {
    setTimeout(() => {
      axios.get("http://127.0.0.1:8000/account/api/users").then((Res) => {
        //  this.setState({name_list:['suraj']})
        var id = localStorage.getItem("id");
        console.log(id);
        var names_list = [];
        var names_id = [];
        var names = Res.data;
        console.log("name is ==========", names);
        names.map((el) => {
          names_list.push({ value: el["id"], label: el["username"] });
        });

        console.log(names_list);
        console.log("param is +++++++++++", this.props.match.params["id"]);
        this.setState({ name: names_list, owner: id });
        const schedule_id_to_update = this.props.match.params["id"];
        var schedule = this.props.scheduletoUpdate.filter((el) => {
          if (el["id"] == schedule_id_to_update) {
            return el;
          }
        });

        console.log("schedule is %%%%%%%%%%", schedule);
        var rjs = [];

        schedule[0].rj.filter((el) => {
          names_list.filter((name) => {
            if (el == name.value) {
              rjs.push({ value: el, label: name.label });
            }
          });
        });
        console.log("updated rjs are ------------", rjs);
        this.setState({
          title: schedule[0].title,
          start_time: new Date(schedule[0].datetime_field_start),
          end_time: new Date(schedule[0].datetime_field_end),
          owner: schedule[0].owner,
          // images:schedule[0].image,

          mul: rjs,
          update_schedule_id: schedule_id_to_update,
          // mul:[]
          // mul:[]
        });
      });
    }, 1000);
  };
  //THIS FUNCTION IS WHILE VISITING THROUGH THE EDIT CLICK SET INTERVAL TIME TO 0 SEC
  visitThroughLink = () => {
    setTimeout(() => {
      axios.get("http://127.0.0.1:8000/account/api/users").then((Res) => {
        //  this.setState({name_list:['suraj']})
        var id = localStorage.getItem("id");
        console.log(id);
        var names_list = [];
        var names_id = [];
        var names = Res.data;
        console.log("name is ==========", names);
        names.map((el) => {
          names_list.push({ value: el["id"], label: el["username"] });
        });

        console.log(names_list);
        console.log("param is +++++++++++", this.props.match.params["id"]);
        this.setState({ name: names_list, owner: id });
        const schedule_id_to_update = this.props.match.params["id"];
        var schedule = this.props.scheduletoUpdate.filter((el) => {
          if (el["id"] == schedule_id_to_update) {
            return el;
          }
        });

        console.log("schedule is %%%%%%%%%%", schedule);
        var rjs = [];

        schedule[0].rj.filter((el) => {
          names_list.filter((name) => {
            if (el == name.value) {
              rjs.push({ value: el, label: name.label });
            }
          });
        });
        console.log("updated rjs are ------------", rjs);
        this.setState({
          title: schedule[0].title,
          start_time: new Date(schedule[0].datetime_field_start),
          end_time: new Date(schedule[0].datetime_field_end),
          owner: schedule[0].owner,
          // images:schedule[0].image,

          mul: rjs,
          update_schedule_id: schedule_id_to_update,
          // mul:[]
          // mul:[]
        });
      });
    }, 0);
  };
  componentDidMount() {
    var a = this.props.location;
    console.log("+++++++++++++++++=", a);
    try {
      if (this.props.location.data[0].edit) {
        this.props.onShow();
        this.visitThroughLink();
      }
    } catch (error) {
      this.props.onShow();
      this.jpt();
    }
    // if(this.props.location.data[0].edit){
    //   this.props.onShow()
    //   this.visitThroughLink()
    // }else{
    //   this.props.onShow()
    //   this.jpt()
    // }
  }
  timeChangeHandler = (start_time) => this.setState({ start_time });
  timeChangeHandler1 = (end_time) => this.setState({ end_time });

  titlechangeHandler = (event) => {
    var name = event.target.name;
    var value = event.target.value;

    this.setState({
      title: value,
      // file:event.target.files
    });
  };

  fileChangeHandler = (event) => {
    var fil = event.target.files;

    this.setState({ image: fil });
  };
  handleChange = (mul) => {
    this.setState({ mul }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };
  // componentDidUpdate() {
  //   this._isMounted = true;

  //   if (this.props.onClick) {
  //     this.props.onClick(this.state);
  //   }
  // }
  scheduleHandler = (event) => {
    event.preventDefault();
    var mulimage = [...this.state.image];
    var time = [this.state.start_time, this.state.end_time];
    var multiple_select = [];
    this.state.mul.map((el) => {
      multiple_select.push(el["value"]);
    });
    this.props.onUpdateSchedule(
      this.state.title,
      time,
      multiple_select,
      mulimage,
      this.state.image.name,
      this.state.owner,
      this.state.update_schedule_id
    );
    this.props.history.push("/show_schedule");

    // if (this.props.onChange) {
    //   this.props.onChange(this.state);
    // }
  };
  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    var names = this.state.name;
    console.log("name is ", names);
    var rjs = [];
    console.log("edit is", this.props.location);

    //  this.state.mul.filter(el=>{
    //      names.filter(name=>{
    //          if(el==name.value){
    //             console.log("sucees++++++++++++")
    //          }
    //      })
    //  })
    // console.log('schedule   is============',this.props.scheduletoUpdate)
    return (
      <div>
        <Header />
        <SideBar />
        <div className="content-wrapper">
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div
                  className="card mb-3"
                  style={{ padding: "20px", maxWidth: "700px" }}
                >
                  <form style={{ padding: "10px" }}>
                    {this.props.added ? (
                      <h1 style={{ color: "green" }}>
                        Added successfully !!!!!
                      </h1>
                    ) : null}

                    <div className="form-group row">
                      <label
                        htmlFor="inputEmail3"
                        className="col-sm-2 col-form-label"
                      >
                        Title
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          name="title"
                          value={this.state.title}
                          onChange={this.titlechangeHandler}
                          className="form-control"
                          id="inputEmail3"
                          placeholder="title..."
                        />
                      </div>
                    </div>

                    <label>Start Date: </label>
                    <DatePicker
                      selected={this.state.start_time}
                      onChange={this.timeChangeHandler}
                      showTimeSelect
                      dateFormat="Pp"
                    />
                    <br />
                    <input
                      type="hidden"
                      name="owner"
                      value={this.state.owner}
                    />

                    <label>End Date: </label>
                    <DatePicker
                      selected={this.state.end_time}
                      onChange={this.timeChangeHandler1}
                      showTimeSelect
                      dateFormat="Pp"
                    />
                    <Select
                      value={this.state.mul}
                      onChange={this.handleChange}
                      options={names}
                      isMulti
                    />
                    <input type="hidden" name="owner" />
                    <input type="hidden" name="counter" />
                    <div className="form-group row">
                      <label
                        htmlFor="inputPassword3"
                        className="col-sm-2 col-form-label"
                      >
                        Image
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="file"
                          onChange={this.fileChangeHandler}
                          name="image"
                          className="form-control"
                          multiple
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col-sm-3">
                        <button
                          type="button"
                          class="btn btn-outline-info"
                          onClick={this.scheduleHandler}
                        >
                          Update Schedule
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapStateToProps = (state) => {
  return {
    scheduletoUpdate: state.sch.schedule,
    // up_schedule:state.updated_schedule.update_schedule,
    // up_schedule_status:state.updated_schedule.update
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    onShow: () => dispatch(actions.scheduleList()),

    onUpdateSchedule: (
      title,
      dates,
      rj,
      image1,
      image2,
      owner,
      update_schedule_id
    ) =>
      dispatch(
        actions.updatescheduleList(
          title,
          dates,
          rj,
          image1,
          image2,
          owner,
          update_schedule_id
        )
      ),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddSchedule);

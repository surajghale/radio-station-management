import React, { Component } from "react";
import DataTable from "react-data-table-component";
import Aux from "../../hoc/Aux";
import { connect } from "react-redux";
import Header from "../header";
import SideBar from "../sidebar";
import { NavLink, Redirect } from "react-router-dom";
import axios from "axios";
import * as actions from "../../store/actions/actions";

class show_schedule extends Component {
  state = {
    show: false,
    id: null,
    delete: false,
    complete: false,
    com: false,
    jpt: true,
  };
  _isMounted = false;

  componentDidMount() {
    this._isMounted = true;
    console.log("schedulelist");
    setTimeout(() => {
      this.props.onShow();
    }, 1000);
    this.props.onShow();
    console.log(
      "complete state is @@@@@@@@@@@@@@@@@@@@@@@",
      this.state.complete
    );
  }

  deleteHandler = (e, id) => {
    this.props.onDelete(id);
    setTimeout(() => {
      this.props.onShow();
    }, 500);

    // axios.post(`http://127.0.0.1:8000/account/api/delete_schedule_account/${id}`).then(res=>{
    //         console.log('update schedule after delete========== data ',res.data)
    //         if(window.confirm("are you sure?")){
    //         axios.delete(`http://127.0.0.1:8000/account/api/delete_schedule/${id}`).then(res=>{
    //           console.log('delete schedule data ',res.data)
    //           this.setState({delete:true})

    //       }).catch(err=>{
    //         console.log(err)
    //     })}
    //       }).catch(err=>{
    //         console.log(err)
    //     })
  };
  componentDidUpdate() {
    // this.props.onShow()
  }
  openBackDrop = (e, id) => {
    e.preventDefault();
    this.setState({ show: true, id: id });
  };
  closeBackDrop = () => {
    this.setState({ show: false, delete: false });
  };
  eventhandler = (data) => {
    // this._isMounted = true;
    this.setState({ show: false });
    console.log("data is ==============", data);
    //  var profile_id=null
    //  this.props.onProfile(profile_id)
  };
  removeHandler = (data) => {
    this.setState({ delete: false });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    var station = localStorage.getItem("is_station");

    // const data = [{ id: 1, title: 'Conan the Barbarian', year: '1982' ,name:"suraj"}];
    var columns = [
      {
        name: "Title",
        selector: "title",
        sortable: true,
      },
      {
        name: "Start Date",
        selector: "datetime_field_start",
        sortable: true,
        // right: true,
      },
      {
        name: "End Date",
        selector: "datetime_field_end",
        sortable: true,
        // cell:row =><select />,
        // right: true,
      },
      {
        name: "Owner",
        selector: "owner",
        sortable: true,
      },
      {
        name: "Rj",
        // selector:'rj',
        cell: (row) => (
          <select>
            {row.rj.map((el) => {
              return <option value={el}> {el} </option>;
            })}
          </select>
        ),
      },
      {
        name: "Edit",
        cell: (row) => (
          <Aux>
            {station == "True" ? (
              <NavLink
                to={{
                  pathname: "/schedule_update/" + row.id,
                  data: [{ edit: true }], // your data array of objects
                }}
              >
                <button>Edit</button>
              </NavLink>
            ) : null}
          </Aux>
          // <NavLink to={'/update/'+row.id}>Edit</NavLink>
        ),
      },
      {
        name: "Delete",
        cell: (row) => (
          <button onClick={(event) => this.deleteHandler(event, row.id)}>
            Delete
          </button>
        ),
      },
    ];
    console.log("schedule is ", this.props.schedule);
    return (
      <div>
        <Header />
        <SideBar />
        <div className="content-wrapper">
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <DataTable
                  title="Arnold Movies"
                  columns={columns}
                  data={this.props.schedule}
                />
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onShow: () => dispatch(actions.scheduleList()),
    onDelete: (id) => dispatch(actions.deleteSchedule(id)),

    //   onUpdateSchedule:(title,dates,rj,image1,image2,owner,update_schedule_id)=>dispatch(actions.updatescheduleList(title,dates,rj,image1,image2,owner,update_schedule_id))
  };
};
var mapStateToProps = (state) => {
  return {
    schedule: state.sch.schedule,
    //   isEdit:state.sch.edit,
    //   up_schedule:state.updated_schedule.update_schedule,
    //   up_schedule_status:state.updated_schedule.update,
    //   delete_schedule_status:state.delete_schedule.delete,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(show_schedule);

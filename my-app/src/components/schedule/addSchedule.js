import React, { Component } from "react";
import { NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Header from "../../components/header";
import SideBar from "../../components/sidebar";
import DateTimeRangePicker from "@wojtekmaj/react-datetimerange-picker";
import $ from "jquery";
import DatePicker from "react-datepicker";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import * as actions from "../../store/actions/actions";
import Aux from "../../hoc/Aux";
import axios from "axios";
// import { colourOptions } from '../data';
import moment from "react-moment";
import "react-datepicker/dist/react-datepicker.css";
// const animatedComponents = makeAnimated();
class addSchedule extends Component {
  state = {
    formdata: {
      title: {
        value: "",
      },
    },
    name: [],
    image: [],
    owner: null,
    start_time: new Date(),
    end_time: new Date(),
    selectedOption: null,
    schedule_added: this.props.added,
  };

  timeChangeHandler = (start_time) => this.setState({ start_time });
  timeChangeHandler1 = (end_time) => this.setState({ end_time });

  titlechangeHandler = (event) => {
    var name = event.target.name;
    var value = event.target.value;
    var updatedForm = {
      ...this.state.formdata,
    };
    var updadtedElement = {
      ...updatedForm[name],
    };
    updadtedElement.value = value;
    // console.log(updadtedElement)
    updatedForm[name] = updadtedElement;
    this.setState({
      formdata: updatedForm,
      // file:event.target.files
    });
  };

  handleChange = (selectedOption) => {
    this.setState({ selectedOption }, () =>
      console.log(`Option selected:`, this.state.selectedOption)
    );
  };
  fileChangeHandler = (event) => {
    var fil = event.target.files;

    this.setState({ image: fil });
  };
  scheduleHandler = (event) => {
    event.preventDefault();

    var mulimage = [...this.state.image];
    var time = [this.state.start_time, this.state.end_time];
    var multiple_select = [];
    this.state.selectedOption.map((el) => {
      multiple_select.push(el["value"]);
    });
    this.props.onSubmitSchedule(
      this.state.formdata.title.value,
      time,
      multiple_select,
      mulimage,
      this.state.image.name,
      this.state.owner
    );
    this.props.history.push("/dashboard");
  };
  gurkhali = (data) => {
    console.log("data is ############################# ", data);
  };
  componentDidMount() {
    axios.get("http://127.0.0.1:8000/account/api/users").then((Res) => {
      //  this.setState({name_list:['suraj']})
      var id = localStorage.getItem("id");
      console.log(id);
      var names_list = [];
      var names_id = [];
      var names = Res.data;
      console.log("name is ==========", names);
      names.map((el) => {
        names_list.push({ value: el["id"], label: el["username"] });
      });
      console.log(names_list);
      this.setState({ name: names_list, owner: id });
    });
  }
  render() {
    console.log(
      "time is ",
      this.state.start_time,
      this.state.end_time,
      this.state.name
    );
    var names = this.state.name;
    console.log("added is ------------", this.props.added);
    // if(this.props.added){
    //   return <Redirect to="/add_schedule" />
    // }
    return (
      <div>
        <Header />
        <SideBar />
        <div className="content-wrapper">
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div
                  className="card mb-3"
                  style={{ padding: "20px", maxWidth: "700px" }}
                >
                  <form style={{ padding: "10px" }}>
                    {this.props.added ? (
                      <h1 style={{ color: "green" }}>
                        Added successfully !!!!!
                      </h1>
                    ) : null}

                    <div className="form-group row">
                      <label
                        htmlFor="inputEmail3"
                        className="col-sm-2 col-form-label"
                      >
                        Title
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          name="title"
                          value={this.state.formdata.title.value}
                          onChange={this.titlechangeHandler}
                          className="form-control"
                          id="inputEmail3"
                          placeholder="title..."
                        />
                      </div>
                    </div>

                    <label>Start Date: </label>
                    <DatePicker
                      selected={this.state.start_time}
                      onChange={this.timeChangeHandler}
                      showTimeSelect
                      dateFormat="Pp"
                    />
                    <br />
                    <input
                      type="hidden"
                      name="owner"
                      value={this.state.owner}
                    />

                    <label>End Date: </label>
                    <DatePicker
                      selected={this.state.end_time}
                      onChange={this.timeChangeHandler1}
                      showTimeSelect
                      dateFormat="Pp"
                    />
                    <Select
                      value={this.state.selectedOption}
                      onChange={this.handleChange}
                      options={names}
                      isMulti
                    />
                    <input type="hidden" name="owner" />
                    <input type="hidden" name="counter" />
                    <div className="form-group row">
                      <label
                        htmlFor="inputPassword3"
                        className="col-sm-2 col-form-label"
                      >
                        Image
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="file"
                          onChange={this.fileChangeHandler}
                          name="image"
                          className="form-control"
                          multiple
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col-sm-3">
                        <button
                          type="button"
                          class="btn btn-outline-info"
                          onClick={this.scheduleHandler}
                        >
                          Add Schedule
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapStateToProps = (state) => {
  return {
    added: state.added_schedule.added,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    onSubmitSchedule: (title, dates, rj, image1, image2, owner) =>
      dispatch(actions.scheduleSubmit(title, dates, rj, image1, image2, owner)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(addSchedule);

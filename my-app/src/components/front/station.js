import React, { Component } from "react";
import "./station.css";
import { NavLink } from "react-router-dom";
export default class station extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top shadow-sm p-3 mb-5">
          <img
            src={process.env.PUBLIC_URL + "/" + "images/vLogo.png"}
            style={{ width: 35, height: "auto", marginLeft: "7%" }}
            alt
          />
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto w-100 justify-content-end">
              <li className="nav-item active">
                <NavLink to="/station" className="nav-link">
                  Home{" "}
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/station_list_1" className="nav-link">
                  Station List
                </NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contac Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Sign In
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  SignUp
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <section
          className="py-md-4 overflow-hidden"
          role="top-section"
          style={{ marginTop: "-50px" }}
        >
          <article className="container">
            <div className="align-items-center mt-4 justify-content-between row">
              <div className="col-md-5">
                <div className>
                  <h1>Radio Station</h1>
                  <div className="h4 mt-4 text-black font-weight-normal">
                    Make you own online radio station Make you own online radio
                    station Make you own online radio station Make you own
                    online radio station Make you own online radio station
                  </div>
                  <div className="main-cta">
                    <a
                      className="btn btn-success mr-3 btn-lg mt-4 font-weight-semibold"
                      href
                    >
                      Ready to listen
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="fluid-left d-none d-md-block mb-5 shadow-sm p-3 mb-5">
                  <img
                    className="shadow-lg img-rounded"
                    width={700}
                    src={process.env.PUBLIC_URL + "/" + "images/gurkha.png"}
                    alt
                  />
                </div>
              </div>
            </div>
          </article>
        </section>
        <h1 style={{ textAlign: "center" }}>Our Partners</h1>
        <div className="container bg-light">
          <section>
            <div className="row justify-content-center" style={{ padding: 80 }}>
              <div className="col-md-4">
                <div
                  className="card"
                  style={{
                    width: "18rem",
                    boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
                  }}
                >
                  <div className="inner">
                    <img
                      className="card-img-top"
                      src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-4 ">
                <div
                  className="card"
                  style={{
                    width: "18rem",
                    boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
                  }}
                >
                  <div className="inner">
                    <img
                      className="card-img-top"
                      src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div
                  className="card shadow-lg"
                  style={{
                    width: "18rem",
                    boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
                  }}
                >
                  <div className="inner">
                    <img
                      className="card-img-top"
                      src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">
                      Some quick example text to build on the card title and
                      make up the bulk of the card's content.
                    </p>
                    <a href="#" className="btn btn-primary">
                      Go somewhere
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <section className="py-md-col-5 my-lg-5">
          <article className="container">
            <div className="align-items-center justify-content-between row">
              <div className="my-5 col-md-6">
                <img
                  className="img-fluid shadow-lg img-rounded"
                  src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                  alt
                />
              </div>
              <div className="col-md-5">
                <div className="py-md-4">
                  <h2 className="font-weight-bold">jpt</h2>
                  <div className="h5 mt-4 text-black font-weight-normal">
                    nepal is a beautifull country nepal is a beautifull country
                    nepal is a beautifull country nepal is a beautifull country
                    nepal is a beautifull country
                  </div>
                  <ul>
                    <li>nepal</li>
                    <li>nepal</li>
                    <li>nepal</li>
                    <li>nepal</li>
                  </ul>
                </div>
              </div>
            </div>
          </article>
        </section>
        <section className="vavFooter flexRow fWidth">
          <h2>pravav.com</h2>
        </section>
      </div>
    );
  }
}

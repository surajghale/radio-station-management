import React, { Component } from "react";
import { connect } from "react-redux";
// import Updatedschedule from './updateSchedule'
import * as actions from "../../store/actions/actions";
import { NavLink } from "react-router-dom";
class ShowStation extends Component {
  subscribeHandler = (event, id) => {
    var data = id;
    console.log("profile_suscribe is &&&&&&&&&&&&&", data);
    this.props.onSubSubmit(id);
  };
  componentDidMount() {
    console.log("station lis");
    this.props.onShowStation();
  }
  // componentDidUpdate(){
  //   console.log('the schedule list is ',this.props.schedule)
  // }
  render() {
    console.log("the station list is ", this.props.station);
    // var up = <Updatedschedule updatedschedule={this.props.schedule}/>
    // var jpt="jpt"
    return (
      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">
              <h1>Staion list</h1>
            </th>
          </tr>
        </thead>
        {this.props.station.map((el) => (
          <tbody key={el.id}>
            <tr>
              <td>
                <h5>
                  {el.livelink} {el.user_id}
                </h5>
                <h5>
                  <NavLink to={"/profile/" + el.user_id}>Go To Profile</NavLink>
                </h5>
                <h5>
                  <button
                    onClick={(event) => this.subscribeHandler(event, el.id)}
                  >
                    Subscribe
                  </button>
                </h5>
              </td>
            </tr>
            {/* <p>id is{el.id}</p> */}
          </tbody>
        ))}
      </table>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onShowStation: () => dispatch(actions.stationList()),
    onSubSubmit: (profile) => dispatch(actions.subscribeSubmit(profile)),
  };
};
var mapStateToProps = (state) => {
  return {
    station: state.stations.station,
    // isEdit:state.stations.edit,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ShowStation);

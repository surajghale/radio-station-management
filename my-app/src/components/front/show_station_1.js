import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";
import Aux from "../../hoc/Aux";
class ShowStation extends Component {
  state = {
    more_status: false,
    test: 3,
  };
  componentDidMount() {
    console.log("station lis");
    this.props.onShowStation();
  }
  changeMoreStatus = () => {
    this.setState({ more_status: true });
  };
  //
  render() {
    var jpt = [];
    var nepal = this.props.station;
    for (let [i, v] of nepal.entries()) {
      jpt.push(v);
      if (i == 3) {
        break;
      }
    }
    console.log("jpt is ========", jpt);
    console.log("stations is =========", this.props.station);
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top shadow-sm p-3 mb-5">
          <img
            src={process.env.PUBLIC_URL + "/" + "images/vLogo.png"}
            style={{ width: 35, height: "auto", marginLeft: "7%" }}
            alt
          />
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto w-100 justify-content-end">
              <li className="nav-item active">
                <NavLink to="/station" className="nav-link">
                  Home{" "}
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/station_list_1" className="nav-link">
                  Station List
                </NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contac Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Sign In
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  SignUp
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <div className="container" style={{ marginTop: "0%" }}>
          <section>
            <div id="gurkha-top" className="row">
              <div className="col-md-6">
                <h3>Popular Stations</h3>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  {/* <label for="exampleFormControlSelect1">View Schedule By Date</label> */}
                  <select
                    className="form-control"
                    id="exampleFormControlSelect1"
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
            <div id="gurkha" className="row justify-content-center">
              {!this.state.more_status ? (
                <Aux>
                  {" "}
                  {jpt.map((el, i) => {
                    return (
                      <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                        <div className="card" style={{ border: "none" }}>
                          <div
                            className="inner"
                            style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)" }}
                          >
                            <NavLink to={"/station_detail/" + el.user_id}>
                              {" "}
                              <img
                                className="card-img-top img-fluid img-rounded"
                                id="img-ad"
                                src={
                                  process.env.PUBLIC_URL +
                                  "/" +
                                  el.image.replace("/my-app/public/", "")
                                }
                                alt="Card image cap"
                              />
                            </NavLink>
                          </div>
                          <div className="card-body text-center">
                            <h5 className="card-title">{el.livelink}</h5>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </Aux>
              ) : (
                <Aux>
                  {this.props.station.map((el, i) => {
                    return (
                      <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                        <div className="card" style={{ border: "none" }}>
                          <div
                            className="inner"
                            style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)" }}
                          >
                            <NavLink to={"/station_detail/" + el.user_id}>
                              {" "}
                              <img
                                className="card-img-top img-fluid img-rounded"
                                id="img-ad"
                                src={
                                  process.env.PUBLIC_URL +
                                  "/" +
                                  el.image.replace("/my-app/public/", "")
                                }
                                alt="Card image cap"
                              />
                            </NavLink>
                          </div>
                          <div className="card-body text-center">
                            <h5 className="card-title">{el.id}</h5>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </Aux>
              )}

              {/* <div class="col-12 col-md-6 col-lg-3 col-xl-3">
          <div class="card" style="border: none;">
            <div class="inner" style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.1);">
            <img class="card-img-top img-fluid img-rounded" id = "img-ad" src="nepal.png" alt="Card image cap">
          </div>
            <div class="card-body text-center">
              <h5 class="card-title">Card title</h5>
             
            </div>
        
    
          </div> */}
            </div>
            <div
              className="row text-center"
              id="more-section"
              style={{ marginTop: "0px", marginBottom: 0 }}
            >
              <div className="col-md-4" />
              <div className="col-md-4">
                <button
                  className="button button3"
                  onClick={this.changeMoreStatus}
                >
                  Show More
                </button>
              </div>
              <div className="col-md-4" />
            </div>
            <div
              className="row text-center"
              style={{
                marginTop: "0px",
                marginBottom: 50,
                marginLeft: "6rem",
                marginRight: "6rem",
              }}
            >
              <div className="col-md-12">
                <hr style={{ border: "1px solid #e6e6e6" }} />
              </div>
            </div>
            <div id="gurkha-top" className="row">
              <div className="col-md-6">
                <h3>Popular Podcasts</h3>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  {/* <label for="exampleFormControlSelect1">View Schedule By Date</label> */}
                  <select
                    className="form-control"
                    id="exampleFormControlSelect1"
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
            <div id="gurkha" className="row justify-content-center">
              <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                <div className="card" style={{ border: "none" }}>
                  <a href="detail.html">
                    <div
                      className="inner"
                      style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)" }}
                    >
                      <img
                        id="img-ad"
                        className="card-img-top img-fluid img-rounded"
                        src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                        alt="Card image cap"
                      />
                    </div>
                  </a>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                <div className="card" style={{ border: "none" }}>
                  <div
                    className="inner"
                    style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)" }}
                  >
                    <img
                      className="card-img-top img-fluid img-rounded"
                      id="img-ad"
                      src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                <div className="card" style={{ border: "none" }}>
                  <div
                    className="inner"
                    style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)" }}
                  >
                    <img
                      className="card-img-top img-fluid img-rounded"
                      id="img-ad"
                      src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 col-lg-3 col-xl-3">
                <div className="card" style={{ border: "none" }}>
                  <div
                    className="inner"
                    style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)" }}
                  >
                    <img
                      className="card-img-top img-fluid img-rounded"
                      id="img-ad"
                      src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                      alt="Card image cap"
                    />
                  </div>
                  <div className="card-body text-center">
                    <h5 className="card-title">Card title</h5>
                  </div>
                </div>
              </div>
              <div className="row text-center" style={{ marginTop: "0px" }}>
                <div className="col-md-4" />
                <div className="col-md-4">
                  <button className="button button3">Show More</button>
                </div>
                <div className="col-md-4" />
              </div>
              {/* <div class="col-12 col-md-6 col-lg-3 col-xl-3">
         <div class="card" style="border: none;">
           <div class="inner" style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.1);">
           <img class="card-img-top img-fluid img-rounded" id = "img-ad" src="nepal.png" alt="Card image cap">
         </div>
           <div class="card-body text-center">
             <h5 class="card-title">Card title</h5>
            
           </div>
       
   
         </div> */}
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onShowStation: () => dispatch(actions.stationList()),
    onSubSubmit: (profile) => dispatch(actions.subscribeSubmit(profile)),
  };
};
var mapStateToProps = (state) => {
  return {
    station: state.stations.station,
    // isEdit:state.stations.edit,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ShowStation);

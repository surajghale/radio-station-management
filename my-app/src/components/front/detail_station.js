import React, { Component } from "react";
import "./station.css";
import Aux from "../../hoc/Aux";
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";
import axios from "axios";
import { NavLink } from "react-router-dom";

class detail_station extends Component {
  state = {
    image: "",
    sub: false,
    ip_s: null,
    diff: 4000,
    footer: false,
    livelink: null,
    time: null,
    entrydate: null,
    start: null,
    current: null,
    end: null,
    rj: null,
    plays: null,
    title: "jpt12346",
    jpt: 2000,
    rj_image: [],
    mn: 2000,
    schedule_status: false,
    schedule_id: null,
    schedule: null,
    program_date: new Date(),
    next_schedule: null,
    sub_status: null,
    sub_click: false,
    edit: false,
    play: false,
  };
  audio = new Audio("https://radio-broadcast.ekantipur.com/stream");
  togglePlay = () => {
    this.props.onCount();
    if (!this.state.play) {
      setTimeout(() => {
        this.props.onIpSubmit(
          this.props.ip_address.query,
          this.props.ip_address.country,
          new Date(),
          this.props.match.params.id
        );
        console.log("ip and location are *************************");
      }, 1000);
    }

    // return this.player.play();
    this.setState({ play: !this.state.play }, () => {
      this.state.play ? this.audio.play() : this.audio.pause();
    });
  };
  subscribeHandler = (event, id) => {
    var data = id;

    console.log("profile_suscribe is &&&&&&&&&&&&&", data);
    this.props.onSubSubmit(id);
    this.setState({ sub: true, sub_click: true });
  };
  editHandler = () => {
    this.setState({ edit: true });
  };
  uneditHandler = () => {
    this.setState({ edit: false });
  };
  unsubscribeHandler = (event, id) => {
    // event.prevenDefault()
    var data = id;
    var log_id = localStorage.getItem("id");
    console.log("profile_suscribe is &&&&&&&&&&&&&", data);
    this.props.onUnSubscribeHandler(id, log_id);
    // this.setState({sub:true,sub_click:true})
  };
  recur = (date) => {
    // console.log('recursive element is ============== ',el)
    // console.log("wow===========wowo")
    var diff = [900000];
    var end_minute = [];
    var schedule_time = [];
    var start_hour = [];
    var starts = [];
    var title = [];
    var new_m = new Date().getMinutes();
    var new_h = new Date().getHours();
    // console.log('diff is ',el)
    // var schedule = this.state.schedule
    date.map((el) => {
      // var schedule_time=[]

      var end_minutes = el.datetime_field_end;
      // console.log("end date is ",end_minutes)
      var e_min = new Date(end_minutes);
      var start_minutes = el.datetime_field_start;
      var start = new Date(start_minutes);
      var end_m = e_min.getMinutes() - 45;
      var end_h = e_min.getHours() - 5;
      var start_m = start.getMinutes() - 45;
      var start_h = start.getHours() - 5;
      // console.log("start is  ",start_h,end_h)
      if (start_m < 0) {
        start_m = start_m + 60;
        start_h = start_h - 1;
      }
      if (start_h <= 0) {
        start_h = start_h + 24;
      }
      if (end_m <= 0) {
        end_m = end_m + 60;
        end_h = end_h - 1;
      }
      if (end_h <= 0) {
        end_h = end_h + 24;
      }
      end_minute.push(end_m);
      starts.push(start_m);
      // console.log('schedule hour and new hour is ',start_m,end_m)
      var schedule = start_h + ":" + start_m + "-" + end_h + ":" + end_m;
      schedule_time.push(schedule);
      start_hour.push(start_h);
      title.push(el.title);

      // console.log("schedule lis is =============",schedule_time)
      // var new_hr = ((new_h + 11) % 12 + 1);
      // console.log("new time is ",new_h,start_h)
      // var diff

      // if(start_h==new_h){
      //   console.log('=======================+++++++++++++start and new hour is',start_h,new_h)
      //   console.log('=======================+++++++++++++start and new minutes is',end_m,new_m)
      //     schedules()
      //     this.setState({title:el.title,start:start_h+":"+start_m,end:end_h+":"+end_m,rj:el.rj,schedule_id:2})

      // else if(start_h==28 && new_m>30){
      //   schedules()
      //   this.setState({title:"timro kura"})
      // }
      // else if(start_h==21){
      //   schedules()
      //   this.state.schedule.filter(el=>{
      //     var start_date = new Date(el.datetime_field_start)
      //     var end_date = new Date(el.datetime_field_end)
      //     var start = start_date.getHours()
      //     if(start_h ==21){
      //       var st = start_date.getHours()
      //       var min = start_date.getMinutes()
      //       var time = st+':'+min
      //       var end = end_date.getHours()
      //       var end_min = end_date.getMinutes()
      //       var endtime = end+':'+end_min
      //       var image = el.image
      //       // var rj_img = image.map(img=>{
      //       //   img.replace('/front_react/public/','')
      //       // })
      //       this.setState({title:el.title,start:time,end:endtime,rj:el.rj,schedule_id:2})
      //     }
      //   })
      // this.setState({title:this.state.schedule[0].title})
      // }
      // var start_m= 59
      // if(start_h==new_h){

      //     console.log('start minute is ',new_m,end_m)
      //     schedules()
      //     this.setState({title:el.title})

      //   // if(end_m>=30 && end_m<45){
      //   //   console.log('start minute is ',new_m,end_m)
      //   //   schedules()
      //   //   this.setState({title:el.title})
      //   // }
      //   // if(end_m>=45 && end_m<59){
      //   //   console.log('start minute is ',new_m,end_m)
      //   //   schedules()
      //   //   this.setState({title:el.title})
      //   // }

      //     }else{
      //       console.log("breaking out of the loop=++++++++++++++++++++++++++")
      //       var diffs = 3000
      //       diff.splice(0,1,diffs)
      //     }

      // else{
      //   console.log('start minute is is',start_m,new_m)

      //   this.setState({title:"jpt"})
      // }

      // if (start_h==23){

      // }
    });
    var schedule_index = [];
    var news = 19;
    var e = [];
    console.log(
      "end minutes list and schedule list is ",
      end_minute,
      schedule_time,
      start_hour
    );
    var schedules = () => {
      var diffs = (end_minute[schedule_index] - new_m) * 60000;
      console.log("diff is ", diffs);
      diff.splice(0, 1, diffs);
      // this.setState({title:'hamro kura'})
    };
    start_hour.map((el, i) => {
      if (el == new_h) {
        if (new_m <= end_minute[i] && new_m >= starts[i]) {
          console.log("perfect schedule is +++++++++", end_minute[i], i);
          schedule_index.push(i);
          e.push(end_minute[i]);
          schedules();
          axios
            .get(`http://127.0.0.1:8000/account/api/image/${i}`)
            .then((res) => {
              console.log("image list is", res.data.images);
              var images = res.data.images;
              console.log("rj image are ==========", images);
              this.setState({ rj_image: images });
              // var imgs = images.map(el=>{
              //   el.replace('images/','')
              // })

              // this.setState({image:res.data[0]})
            })
            .catch((err) => {
              console.log(err);
            });
          this.setState({
            title: title[i],
            current: schedule_time[i],
            schedule_id: i,
            next_schedule: schedule_time[i + 1],
          });
          //       var schedules = ()=>{
          //   var diffs = ((end_minute[i]-new_m)*60000)
          //   console.log("diff is ",diffs)
          //   diff.splice(0,1,diffs)
          //   schedules()

          // }
        }
      } else {
        console.log("failed%%%%%%%%%%%%%%%%%%%%%");
      }
    });
    // this.setState({rj:el.rj})
    // var index = e[0]

    // var schedules = ()=>{
    //   var diffs = ((end_m-new_m)*60000)
    //   console.log("diff is ",diffs)
    //   diff.splice(0,1,diffs)
    //   // this.setState({title:'hamro kura'})
    // }
    console.log("subtraction is ", diff[0], schedule_index);
    setTimeout(() => {
      this.setState({ time: new Date() });
      this.recur(date);
    }, diff[0]);
  };
  componentDidMount() {
    var log_id = localStorage.getItem("id");
    axios
      .get(
        `http://127.0.0.1:8000/account/api/sub_check/${this.props.match.params.id}/${log_id}`
      )
      .then((res) => {
        console.log("subscribe status ", res.data.message);
        var sch = res.data.schedule;
        // var login_user_id = localStorage.getItem('id')
        // if (login_user_id==this.props.match.params.id){
        this.setState({ sub_status: res.data.message });
        // }
      });
    this.props.onProfile(this.props.match.params.id);
    // this.props.onProfile(this.props.match.params.id);
    // var id = localStorage.getItem('id')
    axios
      .get(
        `http://127.0.0.1:8000/account/api/show_schedule/${this.props.match.params.id}`
      )
      .then((res) => {
        console.log("profile schedule is ", res.data.schedule);
        var sch = res.data.schedule;
        this.setState({ schedule: sch, schedule_status: true });
        console.log("jpt is", sch);
        this.recur(sch);
      });

    //  this.setState({schedule:jpt})

    //           // var new_hr = ((new_h + 11) % 12 + 1);

    // axios.get(`http://127.0.0.1:8000/account/api/show_schedule/${id}`).then(res=>{
    //        console.log("profile schedule is ",res.data.schedule)
    //        var date = res.data.schedule})
    var s_id = this.state.schedule_id;
    console.log("schedule id is", s_id);
    //   axios.get(`http://127.0.0.1:8000/account/api/image/${s_id}`).then(res=>{
    //   console.log("image list is",res.data.images)
    //   var images = res.data.images
    //   this.setState({rj_image:images})
    //   // var imgs = images.map(el=>{
    //   //   el.replace('images/','')
    //   // })

    //   // this.setState({image:res.data[0]})
    // }).catch(err=>{
    //   console.log(err)
    // })
    window.addEventListener("beforeunload", this.unOnload);
    window.removeEventListener("beforeunload", this.onUnload);
  }
  render() {
    console.log(
      "detail =++++++++++++",
      this.state.sub_status,
      this.state.sub,
      this.props.delete_sub_status
    );
    // if(this.state.schedule_status){

    //   this.state.schedule.filter(el=>{
    //    console.log(el.owner)
    //   })
    // }

    // var image = this.props.profile_image
    console.log("delete status", this.props.delete_sub_status);
    console.log(
      "subscriber  is--------------------",
      this.props.total_subscriber,
      this.props.sub
    );
    return (
      <Aux>
        <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top shadow-sm p-3 mb-5">
          <img
            src={process.env.PUBLIC_URL + "/" + "images/vLogo.png"}
            style={{ width: 30, height: "auto", marginLeft: "7%" }}
            alt
          />
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto w-100 justify-content-end">
              <li className="nav-item active">
                <NavLink to="/station" className="nav-link">
                  Home{" "}
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/station_list_1" className="nav-link">
                  Station List
                </NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contac Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Sign In
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  SignUp
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <section id="detail123">
          <div className="row">
            <div className="col-md-3 col-sm-3">
              <div
                className="card"
                id="card"
                style={{ border: "0px", backgroundColor: "#f2f2f2" }}
              >
                {this.props.image_status ? (
                  <div
                    className="inner"
                    style={{ boxShadow: "0 4px 8px 0 rgba(0,0,0,0)" }}
                  >
                    <img
                      id="img-ad"
                      className="card-img-top img-fluid img-rounded"
                      src={
                        process.env.PUBLIC_URL +
                        "/" +
                        this.props.profile_image.image
                      }
                      alt="Card image cap"
                    />
                  </div>
                ) : null}

                <div style={{ marginBottom: "-30px" }}>
                  <div className="card-body text-center">
                    {this.props.image_status && !this.state.sub_click ? (
                      <div>
                        <h5>{this.props.profile_image.counter} Subscribers</h5>
                        <h5 style={{ marginTop: "-12px" }}>
                          {this.props.profile_image.livelink}
                        </h5>
                      </div>
                    ) : null}
                    {this.props.image_status &&
                    this.props.sub.clicked &&
                    this.state.sub_click ? (
                      <div>
                        <h5>
                          {this.props.total_subscriber.num_subscriber}{" "}
                          Subscribers
                        </h5>
                        <h5 style={{ marginTop: "-12px" }}></h5>
                        <h5 style={{ marginTop: "-12px" }}>
                          {this.props.profile_image.livelink}
                        </h5>
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-8 col-sm-8">
              <div
                className="card"
                id="card"
                style={{
                  border: "none",
                  backgroundColor: "#f2f2f2",
                  boxShadow:
                    "box-shadow: 0 0 1px rgba(0, 0, 0, 0), 0 1px 3px rgba(0, 0, 0, 0);",
                }}
              >
                <div>
                  {!this.state.edit &&
                  this.state.sub_status == "subscribed" &&
                  !this.state.sub &&
                  this.props.delete_sub_status ? (
                    <button
                      id="status"
                      onClick={(event) => {
                        this.subscribeHandler(
                          event,
                          this.props.match.params.id
                        );
                        this.editHandler();
                      }}
                      className="button button3"
                    >
                      <span>Subscribe case5</span>
                    </button>
                  ) : null}
                  {!this.state.edit &&
                  this.state.sub_status == "subscribed" &&
                  this.state.sub &&
                  this.props.delete_sub_status ? (
                    <button
                      id="status"
                      onClick={(event) => {
                        this.subscribeHandler(
                          event,
                          this.props.match.params.id
                        );
                        this.editHandler();
                      }}
                      className="button button3"
                    >
                      <span>Subscribe case7</span>
                    </button>
                  ) : null}
                  {this.state.edit &&
                  this.state.sub_status == "subscribed" &&
                  this.state.sub &&
                  this.props.delete_sub_status ? (
                    <button
                      id="status"
                      onClick={(event) => {
                        this.unsubscribeHandler(
                          event,
                          this.props.match.params.id
                        );
                        this.uneditHandler();
                      }}
                      className="button button3"
                    >
                      <span>Subscribed case6</span>
                    </button>
                  ) : null}
                  {this.state.sub_status == "subscribed" &&
                  !this.state.sub &&
                  !this.props.delete_sub_status ? (
                    <button
                      id="status"
                      onClick={(event) =>
                        this.unsubscribeHandler(
                          event,
                          this.props.match.params.id
                        )
                      }
                      className="button button3"
                    >
                      <span>Subscribed ref</span>
                    </button>
                  ) : null}
                  {this.state.sub_status == "unsubscribed" &&
                  this.state.sub &&
                  !this.props.delete_sub_status ? (
                    <button
                      id="status"
                      onClick={(event) => {
                        this.unsubscribeHandler(
                          event,
                          this.props.match.params.id
                        );
                        this.editHandler();
                      }}
                      className="button button3"
                    >
                      <span>Subscribed case 2</span>
                    </button>
                  ) : null}
                  {!this.state.edit &&
                  this.state.sub_status == "unsubscribed" &&
                  this.state.sub &&
                  this.props.delete_sub_status ? (
                    <button
                      onClick={(event) => {
                        this.unsubscribeHandler(
                          event,
                          this.props.match.params.id
                        );
                        this.editHandler();
                      }}
                      className="button button3"
                    >
                      Subscribed case 3
                    </button>
                  ) : null}
                  {this.state.edit &&
                  this.state.sub_status == "unsubscribed" &&
                  this.state.sub &&
                  this.props.delete_sub_status ? (
                    <button
                      onClick={(event) => {
                        this.subscribeHandler(
                          event,
                          this.props.match.params.id
                        );
                        this.uneditHandler();
                      }}
                      className="button button3"
                    >
                      Subscribe case 4
                    </button>
                  ) : null}
                  {!this.state.edit &&
                  this.state.sub_status == "unsubscribed" &&
                  !this.state.sub &&
                  !this.props.delete_sub_status ? (
                    <button
                      onClick={(event) =>
                        this.subscribeHandler(event, this.props.match.params.id)
                      }
                      className="button button3"
                    >
                      Subscribe case1
                    </button>
                  ) : null}

                  {/* <button onClick={(event)=>this.subscribeHandler(event,this.props.match.params.id)} className="button button3">Subscribe</button> */}
                  <button className="button button3">Share</button>
                </div>
                <div>
                  <h3>{this.state.title}</h3>
                </div>
                <hr />
                <div>
                  <p>
                    start schedule-&gt; 7:30 - 8:30 | end schedule-&gt; 7:30 -
                    8:30
                  </p>
                  {/* <div>
        <button onClick={this.togglePlay}>{this.state.play ? 'Pause' : 'Play'}</button>
      </div> */}
                  <p>{this.state.current}</p>
                </div>
                <hr />
                <div>{/* <p>Rj: {el.rj}</p> */}</div>
              </div>
            </div>
          </div>
        </section>
        <section className="episode-section">
          <div className="row" style={{ marginBottom: "100px" }}>
            <div className="col-md-7 col-sm-7">
              <div style={{ display: "flex" }}>
                <div className="col-md-6 col-sm-6">
                  <h10>Schedule List</h10>
                </div>
                <div className="col-md-6 col-sm-6">
                  <div className="form-group">
                    {/* <label for="exampleFormControlSelect1">View Schedule By Date</label> */}
                    <select
                      className="form-control"
                      id="exampleFormControlSelect1"
                    >
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                </div>
              </div>
              <hr style={{ marginTop: 0, border: "1px solid grey" }} />
              <div
                className="card text-center"
                style={{ height: 60, boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)" }}
              >
                <div style={{ display: "flex" }}>
                  <img
                    src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                    alt
                    style={{ width: 70, height: 59 }}
                  />
                  <h5 style={{ padding: "10px 10px 30px 30px" }}>
                    title : Gurkha Fm
                  </h5>
                  <p style={{ padding: "10px 30px" }}>schedule 7:00 - 8:00</p>
                </div>
              </div>
              <div
                className="card text-center"
                style={{ height: 60, boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)" }}
              >
                jpt1
              </div>
              <div
                className="card text-center"
                style={{ height: 60, boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)" }}
              >
                jpt1
              </div>
              <div
                className="card text-center"
                style={{ height: 60, boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)" }}
              >
                jpt1
              </div>
              {/* <div id="show" style={{display: 'none'}}>
          <div className="card text-center" style={{height: 60, boxShadow: '0 4px 8px 0 rgba(0,0,0,0.1)'}}>jpt1</div>
          <div className="card text-center" style={{height: 60, boxShadow: '0 4px 8px 0 rgba(0,0,0,0.1)'}}>jpt1</div>
          <div className="card text-center" style={{height: 60, boxShadow: '0 4px 8px 0 rgba(0,0,0,0.1)'}}>jpt1</div>
        </div> */}
              <button
                type="button"
                onclick="showMore()"
                className="btn btn-outline-secondary"
              >
                See More
              </button>
            </div>

            <div className="col-md-4 col-sm-4">
              <h10>Trending Stations ........</h10>
              <hr style={{ border: "1px solid grey", marginTop: 10 }} />
              <div style={{ display: "flex" }}>
                <div className="col-md-6 col-sm-12">
                  <div className="card" id="card">
                    <div className="inner" style={{ height: 80 }}>
                      <img
                        id="img-ad"
                        className="card-img-top img-fluid img-rounded"
                        src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                        alt="Card image cap"
                      />
                    </div>
                    <div>
                      <div
                        className="card-body text-center"
                        style={{ height: 40, marginTop: "-20px" }}
                      >
                        <p style={{ fontSize: 14 }}>Radio Kantipur</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div
                    className="card"
                    id="card"
                    style={{ border: "none", backgroundColor: "white" }}
                  >
                    <div
                      className="inner"
                      style={{
                        boxShadow: "0 4px 8px 0 rgba(0,0,0,0.1)",
                        height: 80,
                      }}
                    >
                      <img
                        id="img-ad"
                        className="card-img-top img-fluid img-rounded"
                        src={process.env.PUBLIC_URL + "/" + "images/nepal.png"}
                        alt="Card image cap"
                      />
                    </div>
                    <div>
                      <div
                        className="card-body text-center"
                        style={{
                          height: 40,
                          marginTop: "-20px",
                          border: "none",
                        }}
                      >
                        <p style={{ fontSize: 14 }}>Radio Kantipur</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* SHOW ALL THE SCHEDULE AND POPULAR RADIO STAIONS ========================== */}
        {/* <section class="play-detail">
  <div class="row text-center" id="play">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      viewBox="0 0 60 60" class="play-style" style="enable-background:new 0 0 60 60;" xml:space="preserve">
   <path d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M45.563,30.826l-22,15
     C23.394,45.941,23.197,46,23,46c-0.16,0-0.321-0.038-0.467-0.116C22.205,45.711,22,45.371,22,45V15c0-0.371,0.205-0.711,0.533-0.884
     c0.328-0.174,0.724-0.15,1.031,0.058l22,15C45.836,29.36,46,29.669,46,30S45.836,30.64,45.563,30.826z"/>
   <g></svg>
    <div class="progress" style="height: 10px;">
      <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div>jpt</div>
  </div> */}
        {/* <div class="col-md-4">
      <div class="progress" style="height: 10px;">
        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
    </div>

  </div>
</section> */}
        <audio id="myAudio">
          <source
            src="https://radio-broadcast.ekantipur.com/stream"
            type="audio/ogg"
          />
          Your browser does not support the audio element.
        </audio>
        <div className="row play text-center sticky-buttom">
          <div className="col-sm-4"></div>
          <div className="col-sm-4">
            {!this.state.play ? (
              <svg
                version="1.1"
                id="Capa_1"
                onClick={this.togglePlay}
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                x="0px"
                y="0px"
                viewBox="0 0 60 60"
                onclick="onPlay()"
                className="play-style"
                style={{ enableBackground: "new 0 0 60 60" }}
                xmlSpace="preserve"
              >
                <path
                  d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M45.563,30.826l-22,15
     C23.394,45.941,23.197,46,23,46c-0.16,0-0.321-0.038-0.467-0.116C22.205,45.711,22,45.371,22,45V15c0-0.371,0.205-0.711,0.533-0.884
     c0.328-0.174,0.724-0.15,1.031,0.058l22,15C45.836,29.36,46,29.669,46,30S45.836,30.64,45.563,30.826z"
                />
                <g />
              </svg>
            ) : (
              <svg
                version="1.1"
                id="Capa_1"
                onClick={this.togglePlay}
                className="play-style"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                x="0px"
                y="0px"
                viewBox="0 0 512 512"
                style={{ enableBackground: "new 0 0 512 512" }}
                xmlSpace="preserve"
              >
                <path
                  d="M256,0C114.617,0,0,114.615,0,256s114.617,256,256,256s256-114.615,256-256S397.383,0,256,0z M224,320
	c0,8.836-7.164,16-16,16h-32c-8.836,0-16-7.164-16-16V192c0-8.836,7.164-16,16-16h32c8.836,0,16,7.164,16,16V320z M352,320
	c0,8.836-7.164,16-16,16h-32c-8.836,0-16-7.164-16-16V192c0-8.836,7.164-16,16-16h32c8.836,0,16,7.164,16,16V320z"
                />
              </svg>
            )}

            <div>
              <h6 style={{ float: "left" }}>live</h6>
              <div style={{ paddingLeft: 30, paddingTop: 12 }}>
                <div className="progress" style={{ height: 2 }}>
                  <div
                    className="progress-bar"
                    role="progressbar"
                    style={{ width: "5%", height: 10 }}
                    aria-valuenow={25}
                    aria-valuemin={0}
                    aria-valuemax={100}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-4"></div>
        </div>
      </Aux>
    );
  }
}

var mapStateToProps = (state) => {
  return {
    // loginId:state.log.userId,
    // isAuthenticated: state.log.login,
    // ip_address:state.ip.userDetail,
    // dates:state.ip.dates,
    // isStation:state.log.station,
    image_status: state.prof.footer,
    ip_address: state.ip.userDetail,
    dates: state.ip.dates,
    profile_image: state.prof.profile,
    total_subscriber: state.subscriber.sub_counter,
    sub: state.subscriber,
    after_delete_sub_counters: state.after_delete_sub_counter.after_sub_delete,
    delete_sub_status: state.after_delete_sub_counter.delete_status,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    onCount: () => dispatch(actions.hitcount()),
    onProfile: (profile_id) => dispatch(actions.profile(profile_id)),
    onSubSubmit: (profile) => dispatch(actions.subscribeSubmit(profile)),
    onUnSubscribeHandler: (profile_id, logged_user_id) =>
      dispatch(actions.deleteSubscribe(profile_id, logged_user_id)),
    onIpSubmit: (ip, location, listen_date, station) =>
      dispatch(actions.ipSubmit(ip, location, listen_date, station)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(detail_station);

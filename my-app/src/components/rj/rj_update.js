import React, { Component } from "react";
import { connect } from "react-redux";
// import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBInput } from 'mdbreact';
// import { Button } from 'rsuite';
// import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";
import * as actions from "../../store/actions/actions";
import axios from "axios";
import Aux from "../../hoc/Aux";
import "./profile.css";
class AddSchedule extends Component {
  state = {
    edit: false,
  };
  componentDidMount() {}
  profileHandler = (event) => {
    event.preventDefault();
    this.setState({ nepal: false });
  };
  render() {
    return (
      <div>
        <Header />
        <SideBar />
        <div className="content-wrapper">
          <section className="content">
            <div className="container-fluid">
              <div classname="row" style={{ display: "flex", padding: "10px" }}>
                <div className="col-md-6">
                  {/* Widget: user widget style 1 */}
                  <div className="card card-widget widget-user">
                    {/* Add the bg color to the header using any of the bg-* classes */}
                    <div
                      className="widget-user-header text-white"
                      style={{
                        background:
                          'url("../dist/img/photo1.png") center center',
                        maxWidth: "100%",
                        height: "300px",
                      }}
                    ></div>
                  </div>
                  {/* /.widget-user */}
                </div>
                <div className="col-md-6">
                  {/* Widget: user widget style 1 */}
                  <div className="card card-widget widget-user">
                    {/* Add the bg color to the header using any of the bg-* classes */}
                    <div
                      className="widget-user-header text-white"
                      style={{
                        background:
                          'url("../dist/img/photo1.png") center center',
                        maxWidth: "100%",
                        height: "300px",
                      }}
                    ></div>
                  </div>
                  {/* /.widget-user */}
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapStateToProps = (state) => {
  return {};
};
var mapDispatchToProps = (dispatch) => {
  return {
    // onUpdateSchedule:(title,dates,rj,image1,image2,owner,update_schedule_id)=>dispatch(actions.updatescheduleList(title,dates,rj,image1,image2,owner,update_schedule_id)),

    // onProfileUpdate:(image,livelink,owner,counter)=>dispatch(actions.updateProfile(image,livelink,owner,counter)),
    onSingleSchedule: (id) => dispatch(actions.singleSchedule(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddSchedule);

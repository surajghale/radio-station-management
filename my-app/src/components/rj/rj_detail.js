import React, { Component } from 'react'
import Header from '../../components/header'
import SideBar from '../../components/sidebar'
import { connect } from 'react-redux';
import * as actions from '../../store/actions/actions'
import axios from 'axios'
import Aux from '../../hoc/Aux'
import { NavLink } from 'react-router-dom';
import DatePicker from 'react-datepicker'

class rj_list extends Component {
  state={
    title:null,
    name:[],
    image:[],
    owner:null,
    start_time:new Date(),
    end_time:new Date(),
    selectedOption: null,
    schedule_added:this.props.added,
    edit:false,
    data:null,
    rj:null,
    rj_id:null,
    update:false,
    nepal:false,
    gurkha:false,
  }
  fileChangeHandler=(event)=>{
    console.log("file is ",event.target.files)
    this.setState({image:event.target.files})
  }
  titlechangeHandler=event=>{
    var name = event.target.name;
    var value = event.target.value
  
     
     this.setState({
       title:value,
       // file:event.target.files
     }) 
   }
   timeChangeHandler = start_time => this.setState({ start_time })
    timeChangeHandler1 = end_time => this.setState({ end_time })

    jpt=()=>{
      setTimeout(() => {
        this.setState({rj:this.props.single_schedule.single_schedule_data.rj,owner:this.props.single_schedule.single_schedule_data.owner,title:this.props.single_schedule.single_schedule_data.title,start_time:new Date(this.props.single_schedule.single_schedule_data.start_date),end_time:new Date(this.props.single_schedule.single_schedule_data.end_date),image:this.props.single_schedule.single_schedule_data.image})
      },1000);
    }
    componentDidMount(){
        // axios.get(`http://127.0.0.1:8000/account/api/image/`).then(res=>{
        //     console.log("rj details  list are===================",res.data)
        //     var jpt = rest.data
        //     this.setState({data:res.data.Rj_Detail})
        // })
        console.log("update-----------")
        this.setState({update:true})
        this.props.onSingleSchedule(this.props.match.params['id'])
        this.props.onShow()
        this.props.onAllShow()
        this.jpt()

       
    }
    scheduleHandler=(event)=>{
      event.preventDefault();
      this.setState({edit:true,update:false,nepal:true})
   }
   eventHandler = (event)=>{
     event.preventDefault()
     this.setState({edit:false,update:false})
   }
   singleScheduleHandler=(event)=>{
    event.preventDefault();
    
    var mulimage = [...this.state.image]
    var time = [this.state.start_time,this.state.end_time]
    console.log("image is ",typeof(mulimage))
    this.props.onUpdateRj(this.state.title,time,this.state.rj,mulimage,this.state.image.name,this.state.owner,this.props.match.params['id'])
        
 }
 scheduleHandlerupdate=()=>{
   this.setState({gurkha:true,edit:true})
 }
    render() {
      var success = null
      // if(this.state.rj_id == this.props.match.params['id']){
      //   success = this.props.rj_update_status
      // }else{
      //   success = !this.props.rj_update_status
      // }
        console.log("image is ",typeof(this.state.image))
        console.log("single schedule is &&&&&&&&7",this.props.single_schedule,this.state.rj)
        console.log("all scedule is is 000000000000000000000",this.props.all_sche)
        console.log("rj id is ",this.props.match.params['id'])
        console.log("update rj is ",this.props.rj_update.rj)
        console.log("status is --------------------------",this.props.all_edit,this.state.edit,this.props.rj_update_status,this.state.update,this.state.gurkha)
    return ( <Aux>{(() => {
      switch (true) {
        case (this.props.all_edit && !this.state.edit && !this.props.rj_update_status && this.state.update && !this.state.gurkha): return(<div>
        <Header/>
        <SideBar/>
        <div className="content-wrapper">
      <section className="content">
        <div className="container-fluid">
        {this.props.all_sche.map(el=>{
         return (<div>
           {el.rj.map(ele=>{
             if(ele ==this.props.match.params['id']){
               return(<div classname="row" style={{display:"flex",padding:"10px"}} >
   
               <div className="col-md-6">
                 {/* Widget: user widget style 1 */}
                 <div className="card card-widget widget-user">
                   <h1>CASE 1</h1>
                   {/* Add the bg color to the header using any of the bg-* classes */}
                   {/* <div className="widget-user-header text-white" style={{background: `url(${"/"+this.state.image}) center center`,maxWidth:"100%",height:"300px"}}> */}
                   <img src={process.env.PUBLIC_URL + "/"+this.state.image} style={{maxHeight:"300px"}}/>
                   {/* </div> */}
                   {/* <input type="file" id="selectedFile" /> */}
                   
                           {/* /.row */}
                        
                      
                   
                 </div>
                 {/* /.widget-user */}
               </div>
               <div>
                 <div className="card border-secondary mb-3">
                       <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
                       <div className="card-body text-secondary">
                         <h5 className="card-title">Title: {el.title}</h5>
                         <br/>
                         <hr></hr>
                         <h5 className="card-title">Start Schedule - {new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()}</h5> 
                         <br/>
                         <hr></hr>
                         <h5 className="card-title">25 Subscribers</h5> 
                         <br/>
                         <hr></hr>
                         <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>
                     
                       </div>
                     </div>
               
               
                   
                 
                 {/* /.widget-user */}
               </div>
               
               </div>)
             }})}
</div>)})}</div>
</section></div></div>)



case (this.props.all_edit && this.state.edit && !this.props.rj_update_status && !this.state.update && !this.state.gurkha):return(<div>
        <Header/>
        <SideBar/>

        <div className="content-wrapper">
      <section className="content">
        <div className="container-fluid">
        
    <div classname="row" style={{display:"flex",padding:"10px"}} >
<div className="col-md-6">
  {/* Widget: user widget style 1 */}
  <div className="card card-widget widget-user">
  <h1>case 2</h1>

    {/* Add the bg color to the header using any of the bg-* classes */}
    <img src={process.env.PUBLIC_URL + "/"+this.props.single_schedule.single_schedule_data.image} style={{maxHeight:"300px"}}/>
     
    
    
    
            {/* /.row */}
         
       
    
  </div>
  {/* /.widget-user */}
</div>


{/* <div className="container-fluid">
        {this.props.all_sche.map(el=>{
         return (<div>
           {el.rj.map(ele=>{
             if(ele ==this.props.match.params['id']){
               return(<div classname="row" style={{display:"flex",padding:"10px"}} > */}
   
   <div>
  {this.props.all_sche.map(el=>{
    return (<div>
{el.rj.map(ele=>{
             if(ele ==this.props.match.params['id']){
        return(<div className="card border-secondary mb-3">
        <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
        <div className="card-body text-secondary">
          <h5 className="card-title">Title: {el.title}</h5>
          <br/>
          <hr></hr>
          <h5 className="card-title">Start Schedule - {new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()}</h5> 
          <br/>
          <hr></hr>
          <h5 className="card-title">25 Subscribers</h5> 
          <br/>
          <hr></hr>
          {this.state.edit?<button type="button" disabled class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>:          
          <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>
}
      </div>
      </div>)
      }
   })}
</div>)
})}
</div></div>
<div className="row" >
                    <div className="card mb-3" style={{padding:"20px",maxWidth:"700px"}}>
                <form style={{padding:"10px"}}>

          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Title</label>
            <div className="col-sm-10">
              <input type="text" name="title" value={this.state.title}
                onChange={this.titlechangeHandler} className="form-control" id="inputEmail3" placeholder="title..."/>
            </div>
          </div>
          
          <label>Start Date: </label>
          <DatePicker
        selected={this.state.start_time}
        onChange={this.timeChangeHandler}
          showTimeSelect
          dateFormat="Pp"
      />
      <br/>
      <input type="hidden" name="owner" value={this.state.owner}/>

      <label>End Date: </label>
      <DatePicker
        selected={this.state.end_time}
        onChange={this.timeChangeHandler1}
          showTimeSelect
          dateFormat="Pp"
      />
      
          <input type="hidden" name="owner" value={this.state.owner}/>
                         
          <div className="form-group row">
            <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Image</label>
            <div className="col-sm-10">
              <input type="file" 
             onChange={this.fileChangeHandler} name="image"className="form-control"/>
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-3">
            <button type="button" class="btn btn-outline-info" onClick={(event)=>{this.singleScheduleHandler(event);this.eventHandler(event)}}>Update Rj</button>
            </div>
          </div>
        </form>
        </div>
</div>
</div>
</section></div></div>)













case (this.props.all_edit && !this.state.edit && this.props.rj_update_status && !this.state.update && !this.state.gurkha):return(<div>
  <Header/>
  <SideBar/>
  <div className="content-wrapper">
<section className="content">
  <div className="container-fluid">
  
<div classname="row" style={{display:"flex",padding:"10px"}} >

<div className="col-md-6">
{/* Widget: user widget style 1 */}
<div className="card card-widget widget-user">
<h1>CASE 3</h1>
{/* Add the bg color to the header using any of the bg-* classes */}
{/* <div className="widget-user-header text-white" style={{background: `url(${"/"+this.state.image}) center center`,maxWidth:"100%",height:"300px"}}> */}
<img src={process.env.PUBLIC_URL +this.props.rj_update.rj.image_update.image.replace("/my-app/public","")} style={{maxHeight:"300px"}}/>
{/* </div> */}

      {/* /.row */}
   
 

</div>
{/* /.widget-user */}
</div>
<div>
<div className="card border-secondary mb-3">
  <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
  <div className="card-body text-secondary">
    <h5 className="card-title">Title: {this.props.rj_update.rj.updated_schedule.title}</h5>
    <br/>
    <hr></hr>
    <h5 className="card-title">Start Schedule - {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getMinutes()}</h5> 
    <br/>
    <hr></hr>
    <h5 className="card-title">25 Subscribers</h5> 
    <br/>
    <hr></hr>
    <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>

  </div>
</div>




{/* /.widget-user */}
</div>

</div>

</div>
</section></div></div>)
case (this.props.all_edit && this.state.edit && this.props.rj_update_status && !this.state.gurkha):return(<div>
  <Header/>
  <SideBar/>
  
  <div className="content-wrapper">
<section className="content">
  <div className="container-fluid">
  
<div classname="row" style={{display:"flex",padding:"10px"}} >
<div className="col-md-6">
{/* Widget: user widget style 1 */}
<div className="card card-widget widget-user">
<h1>CASE 4</h1>
{/* Add the bg color to the header using any of the bg-* classes */}
<img src={process.env.PUBLIC_URL +this.props.rj_update.rj.image_update.image.replace("/my-app/public","")} style={{maxHeight:"300px"}}/>




      {/* /.row */}
   
 

</div>
{/* /.widget-user */}
</div>
<div>
<div className="card border-secondary mb-3">
  <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
  <div className="card-body text-secondary">
    <h5 className="card-title">Title: {this.props.rj_update.rj.updated_schedule.title}</h5>
    <br/>
    <hr></hr>
    <h5 className="card-title">Start Schedule - {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getMinutes()}</h5> 
    <br/>
    <hr></hr>
    <h5 className="card-title">25 Subscribers</h5> 
    <br/>
    <hr></hr>
    {this.state.edit?<button type="button" disabled class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>:    <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>

}

  </div>
</div>



{/* /.widget-user */}
</div>

</div>
<div className="row" >
              <div className="card mb-3" style={{padding:"20px",maxWidth:"700px"}}>
          <form style={{padding:"10px"}}>

    <div className="form-group row">
      <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Title</label>
      <div className="col-sm-10">
        <input type="text" name="title" value={this.state.title}
          onChange={this.titlechangeHandler} className="form-control" id="inputEmail3" placeholder="title..."/>
      </div>
    </div>
    
    <label>Start Date: </label>
    <DatePicker
  selected={this.state.start_time}
  onChange={this.timeChangeHandler}
    showTimeSelect
    dateFormat="Pp"
/>
<br/>
<input type="hidden" name="owner" value={this.state.owner}/>

<label>End Date: </label>
<DatePicker
  selected={this.state.end_time}
  onChange={this.timeChangeHandler1}
    showTimeSelect
    dateFormat="Pp"
/>

    <input type="hidden" name="owner" value={this.state.owner}/>
                   
    <div className="form-group row">
      <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Image</label>
      <div className="col-sm-10">
        <input type="file" 
       onChange={this.fileChangeHandler} name="image"className="form-control"/>
      </div>
    </div>
    <div className="form-group row">
      <div className="col-sm-3">
      <button type="button" class="btn btn-outline-info" onClick={(event)=>{this.singleScheduleHandler(event);this.eventHandler(event)}}>Update Rj</button>
      </div>
    </div>
  </form>
  </div>
</div>
</div>
</section></div></div>)

case (this.props.all_edit && !this.state.edit && this.props.rj_update_status && this.state.update && !this.state.gurkha): return(<div>
  <Header/>
  <SideBar/>
  <div className="content-wrapper">
<section className="content">
  <div className="container-fluid">
  {this.props.all_sche.map(el=>{
   return (<div>
     {el.rj.map(ele=>{
       if(ele ==this.props.match.params['id']){
         return(<div classname="row" style={{display:"flex",padding:"10px"}} >

         <div className="col-md-6">
           {/* Widget: user widget style 1 */}
           <div className="card card-widget widget-user">
             <h1>CASE 5</h1>
             {/* Add the bg color to the header using any of the bg-* classes */}
             {/* <div className="widget-user-header text-white" style={{background: `url(${"/"+this.state.image}) center center`,maxWidth:"100%",height:"300px"}}> */}
             <img src={process.env.PUBLIC_URL + "/"+this.state.image} style={{maxHeight:"300px"}}/>
             {/* </div> */}
             
             
                     {/* /.row */}
                  
                
             
           </div>
           {/* /.widget-user */}
         </div>
         <div>
           <div className="card border-secondary mb-3">
                 <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
                 <div className="card-body text-secondary">
                   <h5 className="card-title">Title: {el.title}</h5>
                   <br/>
                   <hr></hr>
                   <h5 className="card-title">Start Schedule - {new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()}</h5> 
                   <br/>
                   <hr></hr>
                   <h5 className="card-title">25 Subscribers</h5> 
                   <br/>
                   <hr></hr>
                   <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandlerupdate}>Update Rj</button>
               
                 </div>
               </div>
         
         
             
           
           {/* /.widget-user */}
         </div>
         
         </div>)
       }})}
</div>)})}</div>
</section></div></div>)
case (this.props.all_edit && this.state.edit && this.props.rj_update_status && this.state.update && this.state.gurkha):return(<div>
  <Header/>
  <SideBar/>

  <div className="content-wrapper">
<section className="content">
  <div className="container-fluid">
  
<div classname="row" style={{display:"flex",padding:"10px"}} >
<div className="col-md-6">
{/* Widget: user widget style 1 */}
<div className="card card-widget widget-user">
<h1>case 6</h1>

{/* Add the bg color to the header using any of the bg-* classes */}
<img src={process.env.PUBLIC_URL + "/"+this.props.single_schedule.single_schedule_data.image} style={{maxHeight:"300px"}}/>




      {/* /.row */}
   
 

</div>
{/* /.widget-user */}
</div>
<div>
{this.props.all_sche.map(el=>{
    return (<div>
{el.rj.map(ele=>{
             if(ele ==this.props.match.params['id']){
        return(<div className="card border-secondary mb-3">
        <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
        <div className="card-body text-secondary">
          <h5 className="card-title">Title: {el.title}</h5>
          <br/>
          <hr></hr>
          <h5 className="card-title">Start Schedule - {new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(el.datetime_field_start).getHours()} : {new Date(el.datetime_field_start).getMinutes()}</h5> 
          <br/>
          <hr></hr>
          <h5 className="card-title">25 Subscribers</h5> 
          <br/>
          <hr></hr>
          {this.state.edit?<button type="button" disabled class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>:          
          <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>
}
      </div>
      </div>)
      }
   })}
</div>)
})}
</div></div>
<div className="row" >
                    <div className="card mb-3" style={{padding:"20px",maxWidth:"700px"}}>
                <form style={{padding:"10px"}}>

          <div className="form-group row">
            <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Title</label>
            <div className="col-sm-10">
              <input type="text" name="title" value={this.state.title}
                onChange={this.titlechangeHandler} className="form-control" id="inputEmail3" placeholder="title..."/>
            </div>
          </div>
          
          <label>Start Date: </label>
          <DatePicker
        selected={this.state.start_time}
        onChange={this.timeChangeHandler}
          showTimeSelect
          dateFormat="Pp"
      />
      <br/>
      <input type="hidden" name="owner" value={this.state.owner}/>

      <label>End Date: </label>
      <DatePicker
        selected={this.state.end_time}
        onChange={this.timeChangeHandler1}
          showTimeSelect
          dateFormat="Pp"
      />
      
          <input type="hidden" name="owner" value={this.state.owner}/>
                         
          <div className="form-group row">
            <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Image</label>
            <div className="col-sm-10">
              <input type="file" 
             onChange={this.fileChangeHandler} name="image"className="form-control"/>
            </div>
          </div>
          <div className="form-group row">
            <div className="col-sm-3">
            <button type="button" class="btn btn-outline-info" onClick={(event)=>{this.singleScheduleHandler(event);this.eventHandler(event)}}>Update Rj</button>
            </div>
          </div>
        </form>
        </div>
</div>
</div>
</section></div></div>)


// CASE 77777777777777777777777777777777
case (this.props.all_edit && !this.state.edit && this.props.rj_update_status && !this.state.update && this.state.gurkha):return(<div>
  <Header/>
  <SideBar/>
  <div className="content-wrapper">
<section className="content">
  <div className="container-fluid">
  
<div classname="row" style={{display:"flex",padding:"10px"}} >

<div className="col-md-6">
{/* Widget: user widget style 1 */}
<div className="card card-widget widget-user">
<h1>CASE 7</h1>
{/* Add the bg color to the header using any of the bg-* classes */}
{/* <div className="widget-user-header text-white" style={{background: `url(${"/"+this.state.image}) center center`,maxWidth:"100%",height:"300px"}}> */}
<img src={process.env.PUBLIC_URL +this.props.rj_update.rj.image_update.image.replace("/my-app/public","")} style={{maxHeight:"300px"}}/>
{/* </div> */}

      {/* /.row */}
   
 

</div>
{/* /.widget-user */}
</div>
<div>
<div className="card border-secondary mb-3">
  <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
  <div className="card-body text-secondary">
    <h5 className="card-title">Title: {this.props.rj_update.rj.updated_schedule.title}</h5>
    <br/>
    <hr></hr>
    <h5 className="card-title">Start Schedule - {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getMinutes()}</h5> 
    <br/>
    <hr></hr>
    <h5 className="card-title">25 Subscribers</h5> 
    <br/>
    <hr></hr>
    <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>

  </div>
</div>




{/* /.widget-user */}
</div>

</div>

</div>
</section></div></div>)

//CASE 8888888888888888888888888888

case (this.props.all_edit && this.state.edit && this.props.rj_update_status && this.state.gurkha):return(<div>
  <Header/>
  <SideBar/>
  
  <div className="content-wrapper">
<section className="content">
  <div className="container-fluid">
  
<div classname="row" style={{display:"flex",padding:"10px"}} >
<div className="col-md-6">
{/* Widget: user widget style 1 */}
<div className="card card-widget widget-user">
<h1>CASE 8</h1>
{/* Add the bg color to the header using any of the bg-* classes */}
<img src={process.env.PUBLIC_URL +this.props.rj_update.rj.image_update.image.replace("/my-app/public","")} style={{maxHeight:"300px"}}/>




      {/* /.row */}
   
 

</div>
{/* /.widget-user */}
</div>
<div>
<div className="card border-secondary mb-3">
  <div className="card-header" style={{maxWidth:"100%"}}>Rj details</div>
  <div className="card-body text-secondary">
    <h5 className="card-title">Title: {this.props.rj_update.rj.updated_schedule.title}</h5>
    <br/>
    <hr></hr>
    <h5 className="card-title">Start Schedule - {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_start).getMinutes()} |  Schedule Ends -{new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getHours()} : {new Date(this.props.rj_update.rj.updated_schedule.datetime_field_end).getMinutes()}</h5> 
    <br/>
    <hr></hr>
    <h5 className="card-title">25 Subscribers</h5> 
    <br/>
    <hr></hr>
    {this.state.edit?<button type="button" disabled class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>:    <button type="button" class="btn btn-outline-info" onClick={this.scheduleHandler}>Update Rj</button>

}

  </div>
</div>



{/* /.widget-user */}
</div>

</div>
<div className="row" >
              <div className="card mb-3" style={{padding:"20px",maxWidth:"700px"}}>
          <form style={{padding:"10px"}}>

    <div className="form-group row">
      <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Title</label>
      <div className="col-sm-10">
        <input type="text" name="title" value={this.state.title}
          onChange={this.titlechangeHandler} className="form-control" id="inputEmail3" placeholder="title..."/>
      </div>
    </div>
    
    <label>Start Date: </label>
    <DatePicker
  selected={this.state.start_time}
  onChange={this.timeChangeHandler}
    showTimeSelect
    dateFormat="Pp"
/>
<br/>
<input type="hidden" name="owner" value={this.state.owner}/>

<label>End Date: </label>
<DatePicker
  selected={this.state.end_time}
  onChange={this.timeChangeHandler1}
    showTimeSelect
    dateFormat="Pp"
/>

    <input type="hidden" name="owner" value={this.state.owner}/>
                   
    <div className="form-group row">
      <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Image</label>
      <div className="col-sm-10">
        <input type="file" 
       onChange={this.fileChangeHandler} name="image"className="form-control"/>
      </div>
    </div>
    <div className="form-group row">
      <div className="col-sm-3">
      <button type="button" class="btn btn-outline-info" onClick={(event)=>{this.singleScheduleHandler(event);this.eventHandler(event)}}>Update Rj</button>
      </div>
    </div>
  </form>
  </div>
</div>
</div>
</section></div></div>)
default: return (null);
        }
      })()}
      </Aux>)
    }}
var mapDispatchToProps=dispatch=>{
    return {
    onShow:()=>dispatch(actions.scheduleList()),
    onAllShow:()=>dispatch(actions.all_schedules()),
    onSingleSchedule:(id)=>dispatch(actions.singleSchedule(id)),
    onUpdateRj:(title,dates,rj,image1,image2,owner,update_schedule_id)=>dispatch(actions.updateRj(title,dates,rj,image1,image2,owner,update_schedule_id))


    //   onUpdateSchedule:(title,dates,rj,image1,image2,owner,update_schedule_id)=>dispatch(actions.updatescheduleList(title,dates,rj,image1,image2,owner,update_schedule_id))
    }
  }
  var mapStateToProps = state=>{
    return{
      rj_images:state.rj_details.rj,
      schedule:state.sch.schedule,
      edit:state.sch.edit,
      all_sche:state.all.schedule,
      all_edit:state.all.edit,
      single_schedule:state.single,
      rj_update:state.rj_update,
      rj_update_status:state.rj_update.rj_status
    }
  }
  export default connect( mapStateToProps, mapDispatchToProps )( rj_list );
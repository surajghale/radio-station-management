import React, { Component } from "react";
import { NavLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Header from "../../components/header";
import SideBar from "../../components/sidebar";
import DateTimeRangePicker from "@wojtekmaj/react-datetimerange-picker";
import $ from "jquery";
import DatePicker from "react-datepicker";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import * as actions from "../../store/actions/actions";
import Aux from "../../hoc/Aux";
import axios from "axios";
// import { colourOptions } from '../data';
import moment from "react-moment";
import "react-datepicker/dist/react-datepicker.css";
// const animatedComponents = makeAnimated();
class addSchedule extends Component {
  state = {
    formdata: {
      username: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
        error: "",
        touched: false,
      },
      email: {
        value: "",
        valid: false,
        validation: {
          required: true,
          isEmail: true,
        },
        error: "",
      },
      password: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
        error: "",
      },
      password2: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
        touched: false,
        error: "",
      },
    },
    image: null,
  };

  changeHandler = (event) => {
    var name = event.target.name;
    var value = event.target.value;
    var updatedForm = {
      ...this.state.formdata,
    };
    var updadtedElement = {
      ...updatedForm[name],
    };
    updadtedElement.value = value;
    // console.log(updadtedElement)
    updatedForm[name] = updadtedElement;
    this.setState({
      formdata: updatedForm,
      // file:event.target.files
    });
  };

  fileChangeHandler = (event) => {
    var fil = event.target.files[0];

    this.setState({ image: fil });
  };
  scheduleHandler = (event) => {
    //   event.preventDefault();
    //   this.props.onSubmitSchedule(this.state.formdata.title.value,time,multiple_select,mulimage,this.state.image.name,this.state.owner)
    //   this.props.history.push('/dashboard')
  };
  gurkhali = (data) => {
    console.log("data is ############################# ", data);
  };
  registerHandler = (e) => {
    e.preventDefault();
    this.props.onAuth(
      false,
      this.state.formdata.username.value,
      this.state.formdata.email.value,
      this.state.formdata.password.value,
      this.state.formdata.password2.value,
      null,
      this.state.image,
      this.state.image.name
    );
  };
  componentDidMount() {
    axios.get("http://127.0.0.1:8000/account/api/users").then((Res) => {
      //  this.setState({name_list:['suraj']})
      var id = localStorage.getItem("id");
      console.log(id);
      var names_list = [];
      var names_id = [];
      var names = Res.data;
      console.log("name is ==========", names);
      names.map((el) => {
        names_list.push({ value: el["id"], label: el["username"] });
      });
      console.log(names_list);
      this.setState({ name: names_list, owner: id });
    });
  }
  render() {
    return (
      <div>
        <Header />
        <SideBar />
        <div className="content-wrapper">
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div
                  className="card mb-6"
                  style={{ padding: "20px", maxWidth: "900px" }}
                >
                  <form style={{ width: "600px", padding: "10px" }}>
                    <div className="form-group row">
                      <label
                        htmlFor="inputEmail3"
                        className="col-sm-2 col-form-label"
                      >
                        Username
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          name="username"
                          value={this.state.formdata.username.value}
                          onChange={this.changeHandler}
                          className="form-control"
                          id="inputEmail3"
                          placeholder="Username..."
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label
                        htmlFor="inputEmail3"
                        className="col-sm-2 col-form-label"
                      >
                        Email
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="email"
                          name="email"
                          value={this.state.formdata.email.value}
                          onChange={this.changeHandler}
                          className="form-control"
                          id="inputEmail3"
                          placeholder="Email..."
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label
                        htmlFor="inputEmail3"
                        className="col-sm-2 col-form-label"
                      >
                        password
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="password"
                          name="password"
                          value={this.state.formdata.password.value}
                          onChange={this.changeHandler}
                          className="form-control"
                          id="inputEmail3"
                          placeholder="password..."
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label
                        htmlFor="inputEmail3"
                        className="col-sm-2 col-form-label"
                      >
                        Conform Password
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="password"
                          name="password2"
                          value={this.state.formdata.password2.value}
                          onChange={this.changeHandler}
                          className="form-control"
                          id="inputEmail3"
                          placeholder="conform password..."
                        />
                      </div>
                    </div>

                   

                    <input
                      type="hidden"
                      name="owner"
                      value={this.state.owner}
                    />

                    <input type="hidden" name="owner" />
                    <input type="hidden" name="counter" />
                    <div className="form-group row">
                      <label
                        htmlFor="inputPassword3"
                        className="col-sm-2 col-form-label"
                      >
                        Image
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="file"
                          onChange={this.fileChangeHandler}
                          name="image"
                          className="form-control"
                          multiple
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col-sm-3">
                        <button
                          type="button"
                          style={{ padding: "5px 20px" }}
                          class="btn btn-outline-info"
                          onClick={this.registerHandler}
                        >
                          Add Rj
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapStateToProps = (state) => {
  return {
    isregistered: state.regis.registered,
    username: state.regis.username,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (
      station,
      username,
      email,
      password,
      password2,
      livelink,
      image1,
      image2
    ) =>
      dispatch(
        actions.register(
          station,
          username,
          email,
          password,
          password2,
          livelink,
          image1,
          image2
        )
      ),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(addSchedule);

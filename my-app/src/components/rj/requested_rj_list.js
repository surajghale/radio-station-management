import React, { Component } from "react";
import DataTable from "react-data-table-component";
import Aux from "../../hoc/Aux";
import { connect } from "react-redux";
import Header from "../header";
import SideBar from "../sidebar";
import { NavLink, Redirect } from "react-router-dom";
import axios from "axios";
import * as actions from "../../store/actions/actions";

class requested_rj_list extends Component {
  state = {
    show: false,
    id: null,
    delete: false,
    complete: false,
    com: false,
    jpt: true,
    title: null,
    image: [],
    time: [],
    rj: null,
    click_approve: false,
    request: false,
  };
  _isMounted = false;

  componentDidMount() {
    this._isMounted = true;
    console.log("schedulelist");
    if (this.state.request) {
      console.log("success~~~~~~~~~~~~~~~~~~");
    }
    setTimeout(() => {
      this.props.onShow();
    }, 1000);
    this.props.onShow();
    console.log(
      "complete state is @@@@@@@@@@@@@@@@@@@@@@@",
      this.state.complete
    );
  }
  deleteHandler = (e, id) => {
    axios
      .delete(`http://127.0.0.1:8000/account/api/delete_rj_request/${id}`)
      .then((res) => {
        console.log("delete status ", res.data);
        setTimeout(() => {
          this.props.onShow();
        }, 1000);
      });
  };
  componentDidUpdate() {
    // this.props.onShow()
  }
  openBackDrop = (e, id) => {
    e.preventDefault();
    this.setState({ show: true, id: id });
  };
  closeBackDrop = () => {
    this.setState({ show: false, delete: false });
  };
  eventhandler = (data) => {
    // this._isMounted = true;
    this.setState({ show: false });
    console.log("data is ==============", data);
  };
  removeHandler = (data) => {
    this.setState({ delete: false });
  };
  jpt = () => {
    setTimeout(() => {
      this.setState({ request: true });
    }, 1000);
  };
  request_delete = (id) => {
    axios
      .delete(`http://127.0.0.1:8000/account/api/delete_rj_request/${id}`)
      .then((res) => {
        console.log("delete status ", res.data);
        setTimeout(() => {
          this.props.onShow();
        }, 1000);
      });
  };
  updateScheduleHandler = (e, id, title, rj, start, end, owner, image) => {
    e.preventDefault();
    // this.props.onSingleRequest(rj)
    var formData = new FormData();

    var mulimage = [image];
    var rjs = [11, 12];
    console.log("rj is", rjs);
    var jpt = [];
    rjs.map((el) => {
      jpt.push(el);
    });
    var time = [new Date(start), new Date(end)];
    var start = time[0];
    var end = time[1];
    console.log("date is ====++++++++++", start, end);
    var standardEndFormat = `${end.getFullYear()}-${
      end.getMonth() + 1
    }-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`;
    var standardStartFormat = `${start.getFullYear()}-${
      start.getMonth() + 1
    }-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`;
    // var getfullyear = start
    console.log("date and time is", standardStartFormat, standardEndFormat);
    // var m_image = image1
    // if(m_image.length==1){
    //     formData.append('image',m_image[0])
    // }
    // else if(m_image.length==2){
    //     formData.append('image',m_image[0])
    //     formData.append('image',m_image[1])
    // }
    // console.log(typeof dates,time[0])
    // formData.append('image',image)

    formData.append("title", title);
    formData.append("datetime_field_start", standardStartFormat);
    formData.append("datetime_field_end", standardEndFormat);
    formData.append("rjs", [[27, 28]]);
    formData.append("image", image);
    // formData.append('image',image1,image2)
    formData.append("owner", owner);
    // formData.append('timefield',time)
    console.log("the real rj is ", [11, 12]);
    console.log("form data is ", formData);
    var jpt = rj;
    console.log("scheudle_update_id is ", jpt);
    // var schedule_to_update = props.match.params
    // console.log(schedule_to_update)
    var id = localStorage.getItem("id");
    // console.log("detail single +++++++++++++++++",title,standardStartFormat,standardEndFormat,rj,image1,owner,update_schedule_id)
    axios
      .put(`http://127.0.0.1:8000/account/api/approve_rj/${jpt}`, formData)
      .then((res) => {
        console.log("update schedule are", res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    this.request_delete(rj);
  };
  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    var station = localStorage.getItem("is_station");
    console.log(
      "single reqeust is +++++++++++",
      this.props.single_request,
      this.state.request
    );
    console.log("requested list are +++++++++++++", this.props.request_list);
    // const data = [{ id: 1, title: 'Conan the Barbarian', year: '1982' ,name:"suraj"}];
    var columns = [
      {
        name: "Title",
        selector: "title",
        sortable: true,
      },
      {
        name: "Start Date",
        selector: "datetime_field_start",
        sortable: true,
        // right: true,
      },
      {
        name: "Owner",
        selector: "owner",
        sortable: true,
        // cell:row =><select />,
        // right: true,
      },
      {
        name: "Rj",
        selector: "rj",
        sortable: true,
      },

      {
        name: "Approve",
        cell: (row) => (
          <button
            onClick={(event) =>
              this.updateScheduleHandler(
                event,
                row.id,
                row.title,
                row.rj,
                row.datetime_field_start,
                row.datetime_field_end,
                row.owner,
                row.image
              )
            }
          >
            Approve
          </button>
        ),
      },
      {
        name: "DisApprove",
        cell: (row) => (
          <button onClick={(event) => this.deleteHandler(event, row.id)}>
            DisApprove
          </button>
        ),
      },
    ];
    console.log("schedule is ", this.props.schedule);
    return (
      <div>
        <Header />
        <SideBar />
        <div className="content-wrapper">
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                {this.state.request ? (
                  <input
                    type="image"
                    id="image"
                    alt="Login"
                    src="jpt"
                    onChange={this.imageHandler}
                    value={this.props.single_request.image}
                  ></input>
                ) : null}

                <DataTable
                  title="Arnold Movies"
                  columns={columns}
                  data={this.props.request_list}
                />
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onShow: () => dispatch(actions.showRjRequest()),
    onSingleRequest: (id) => dispatch(actions.singleRequest(id)),
    onUpdateRj: (title, dates, rj, image1, image2, owner, update_schedule_id) =>
      dispatch(
        actions.updateRj(
          title,
          dates,
          rj,
          image1,
          image2,
          owner,
          update_schedule_id
        )
      ),
  };
};
var mapStateToProps = (state) => {
  return {
    request_list: state.reqeust_list_rj.show_list,
    single_schedule: state.single,
    single_request: state.singles.single_request_data,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(requested_rj_list);

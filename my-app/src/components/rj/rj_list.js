import React, { Component } from "react";
import Header from "../../components/header";
import SideBar from "../../components/sidebar";
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";
import axios from "axios";
import Aux from "../../hoc/Aux";
import { NavLink } from "react-router-dom";
class rj_list extends Component {
  state = {
    data: null,
  };
  componentDidMount() {
    // axios.get(`http://127.0.0.1:8000/account/api/image/`).then(res=>{
    //     console.log("rj details  list are===================",res.data)
    //     var jpt = rest.data
    //     this.setState({data:res.data.Rj_Detail})
    // })

    this.props.onShow();
    this.props.onAllShow();
  }
  render() {
    console.log("all scedule is is 000000000000000000000", this.props.all_sche);
    console.log("rj  image is is 000000000000000000000", this.props.rj_images);

    return (
      <Aux>
        {this.props.all_edit ? (
          <div>
            <Header />
            <SideBar />
            <div className="content-wrapper">
              <section className="content">
                <div className="container-fluid">
                  <div
                    classname="row"
                    style={{
                      display: "flex",
                      width: "100%",
                      flexWrap: "wrap",
                      padding: "10px",
                    }}
                  >
                    {this.props.rj_images.map((el) => {
                      // <NavLink to={'/detail_rj_dash/'+el.rj}></NavLink>
                      return (
                        <div
                          className="col-md-4"
                          style={{
                            display: "flex",
                            height: "auto",
                            width: "auto",
                          }}
                        >
                          {/* Widget: user widget style 1 */}
                          <div
                            className="card card-widget widget-user"
                            style={{ width: "360px" }}
                          >
                            {/* Add the bg color to the header using any of the bg-* classes */}
                            <div
                              className="widget-user-header text-white"
                              style={{
                                background:
                                  'url("../dist/img/photo1.png") center center',
                              }}
                            ></div>
                            <div className="widget-user-image">
                              <NavLink to={"/detail_rj_dash/" + el.rjs}>
                                <img
                                  className="img-circle"
                                  src={
                                    process.env.PUBLIC_URL +
                                    el.image.replace("/my-app/public", "")
                                  }
                                  style={{ height: "115px", width: "115px" }}
                                  alt="User Avatar"
                                />
                              </NavLink>
                            </div>
                            {this.props.all_sche.map((sche) => {
                              if (sche.id == el.schedule) {
                                return (
                                  <div className="card-footer">
                                    <div className="row">
                                      <div className="col-sm-4 border-right">
                                        <div className="description-block">
                                          <h5 className="description-header">
                                            3,200
                                          </h5>
                                          <span className="description-text">
                                            Subscribers
                                          </span>
                                        </div>
                                        {/* /.description-block */}
                                      </div>
                                      {/* /.col */}
                                      <div className="col-sm-4 border-right">
                                        <div className="description-block">
                                          <h5 className="description-header">
                                            Title
                                          </h5>
                                          <span className="description-text">
                                            {sche.title}
                                          </span>
                                        </div>
                                        {/* /.description-block */}
                                      </div>
                                      {/* /.col */}
                                      <div className="col-sm-4">
                                        <div className="description-block">
                                          <h5 className="description-header">
                                            Rj
                                          </h5>
                                          <span className="description-text">
                                            {el.rjs}
                                          </span>
                                        </div>
                                        {/* /.description-block */}
                                      </div>
                                      {/* /.col */}
                                    </div>

                                    {/* /.row */}
                                  </div>
                                );
                              }
                            })}
                          </div>
                          {/* /.widget-user */}
                        </div>
                      );
                    })}
                  </div>
                </div>
              </section>
            </div>
          </div>
        ) : null}
      </Aux>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onShow: () => dispatch(actions.scheduleList()),
    onAllShow: () => dispatch(actions.all_schedules()),

    //   onUpdateSchedule:(title,dates,rj,image1,image2,owner,update_schedule_id)=>dispatch(actions.updatescheduleList(title,dates,rj,image1,image2,owner,update_schedule_id))
  };
};
var mapStateToProps = (state) => {
  return {
    rj_images: state.rj_details.rj,
    schedule: state.sch.schedule,
    edit: state.sch.edit,
    all_sche: state.all.schedule,
    all_edit: state.all.edit,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(rj_list);

import React, { Component } from "react";
import * as actions from "../../store/actions/actions";
import { connect } from "react-redux";
import axios from "axios";
import { Redirect } from "react-router-dom";
class Register extends Component {
  state = {
    formdata: {
      username: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
        error: "",
        touched: false,
      },
      email: {
        value: "",
        valid: false,
        validation: {
          required: true,
          isEmail: true,
        },
        error: "",
      },
      password: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
        error: "",
      },
      password2: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
        touched: false,
        error: "",
      },

      livelink: {
        value: "",
        valid: false,
        validation: {
          required: true,
        },
      },
    },
    image: null,
    is_station: null,
  };
  checkValidity(value, rules) {
    let isValid = true;
    if (!rules) {
      return true;
    }

    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    return isValid;
  }
  changeHandler = (event) => {
    var name = event.target.name;
    var value = event.target.value;
    var updatedForm = {
      ...this.state.formdata,
    };
    var updadtedElement = {
      ...updatedForm[name],
    };
    console.log("update element is ", updadtedElement, name);
    var e1 = "";
    var e2 = "";
    var e3 = "";
    var e4 = "";
    var e5 = "";
    updadtedElement.value = value;
    updadtedElement.valid = this.checkValidity(
      updadtedElement.value,
      updadtedElement.validation
    );
    if (updadtedElement.valid == false) {
      e1 = "Username connot be empty";
      e2 = "invalid email";
      //  e3="week password"
      //  e4="medium password"
      //  e5="strong password"
    }
    if (updadtedElement.valid == true) {
      e3 = "week password";
      e4 = "medium password";
      e5 = "strong password";
    }
    if (name == "username") {
      updadtedElement.error = e1;
      updatedForm[name] = updadtedElement;
      this.setState({ formdata: updatedForm });
    } else if (name == "email") {
      updadtedElement.error = e2;
      updatedForm[name] = updadtedElement;
      this.setState({ formdata: updatedForm });
    } else if (name == "password") {
      switch (true) {
        case updadtedElement.value.length <= 5:
          updadtedElement.error = e3;
          updatedForm[name] = updadtedElement;
          this.setState({ formdata: updatedForm });
          break;

        case updadtedElement.value.length > 5 &&
          updadtedElement.value.length <= 10:
          updadtedElement.error = e4;
          updatedForm[name] = updadtedElement;
          this.setState({ formdata: updatedForm });
          break;
        case updadtedElement.value.length > 10:
          updadtedElement.error = e5;
          updatedForm[name] = updadtedElement;
          this.setState({ formdata: updatedForm });
          break;
        default:
          updatedForm[name] = updadtedElement;
          this.setState({ formdata: updatedForm });
      }
    } else if (name == "password2") {
      updadtedElement.touched = true;

      updatedForm[name] = updadtedElement;
      this.setState({ formdata: updatedForm });
    } else if (name == "livelink") {
      updatedForm[name] = updadtedElement;
      this.setState({ formdata: updatedForm });
    }
  };
  blurHandler = (event) => {
    var name = event.target.name;
    var updatedForm = {
      ...this.state.formdata,
    };
    var updadtedElement = {
      ...updatedForm[name],
    };
    console.log("onblur name is =========", name);
    if (updadtedElement.error == "") {
      updadtedElement.error = `${name} cannot be empty`;
      updatedForm[name] = updadtedElement;
      this.setState({
        formdata: updatedForm,
      });
    }
  };
  fileChangeHandler = (event) => {
    this.setState({ image: event.target.files[0] });
  };
  stationChangeHandler = (event) => {
    this.setState({ is_station: event.target.value });
  };
  registerHandler = (e) => {
    e.preventDefault();
    this.props.onAuth(
      this.state.is_station,
      this.state.formdata.username.value,
      this.state.formdata.email.value,
      this.state.formdata.password.value,
      this.state.formdata.password2.value,
      this.state.formdata.livelink.value,
      this.state.image,
      this.state.image.name
    );
  };
  render() {
    console.log("register is ---------", this.props.isregistered);
    if (this.props.isregistered) {
      return <Redirect to="/" />;
    }
    return (
      <body className="hold-transition login-page">
        <div className="login-box">
          <div className="login-logo">
            <h1>Sign up</h1>
          </div>
          {/* /.login-logo */}
          <div className="card">
            <div className="card-body login-card-body">
              <p className="login-box-msg">Sign up to start your session</p>

              <form action="../../index3.html" method="post">
                <p>Please select your gender:</p>
                <input
                  type="radio"
                  id="male"
                  name="is_station"
                  onChange={this.stationChangeHandler}
                  value="station"
                />
                <label for="male">Station</label>
                <input
                  type="radio"
                  id="female"
                  name="is_station"
                  onChange={this.stationChangeHandler}
                  value="user"
                />
                <label for="female">User</label>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Username"
                    value={this.state.formdata.username.value}
                    onChange={this.changeHandler}
                    name="username"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-envelope" />
                    </div>
                  </div>
                </div>
                <div className="input-group mb-3">
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    value={this.state.formdata.email.value}
                    onChange={this.changeHandler}
                    name="email"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-envelope" />
                    </div>
                  </div>
                </div>
                <div className="input-group mb-3">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    value={this.state.formdata.password.value}
                    onChange={this.changeHandler}
                    name="password"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock" />
                    </div>
                  </div>
                </div>
                <div className="input-group mb-3">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Conform Password"
                    value={this.state.formdata.password2.value}
                    onChange={this.changeHandler}
                    name="password2"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-lock" />
                    </div>
                  </div>
                </div>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="livelink"
                    value={this.state.formdata.livelink.value}
                    onChange={this.changeHandler}
                    name="livelink"
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-book" />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputPassword3"
                    className="col-sm-2 col-form-label"
                  >
                    Image
                  </label>
                  <div className="col-sm-10">
                    <input
                      type="file"
                      onChange={this.fileChangeHandler}
                      name="image"
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-8">
                    <div className="icheck-primary">
                      <input type="checkbox" id="remember" />
                      <label htmlFor="remember">Remember Me</label>
                    </div>
                  </div>
                  {/* /.col */}
                  <div className="col-4">
                    <button
                      type="submit"
                      className="btn btn-primary btn-block"
                      onClick={this.registerHandler}
                    >
                      Sign Up
                    </button>
                  </div>
                  {/* /.col */}
                </div>
              </form>
              <div className="social-auth-links text-center mb-3">
                <p>- OR -</p>
                <a href="#" className="btn btn-block btn-primary">
                  <i className="fab fa-facebook mr-2" /> Sign up using Facebook
                </a>
                <a href="#" className="btn btn-block btn-danger">
                  <i className="fab fa-google-plus mr-2" /> Sign up using
                  Google+
                </a>
              </div>
              {/* /.social-auth-links */}

              <p className="mb-0">
                <a href="register.html" className="text-center">
                  Already Account?Login
                </a>
              </p>
            </div>
            {/* /.login-card-body */}
          </div>
        </div>
      </body>
    );
  }
}
var mapStateToProps = (state) => {
  return {
    isregistered: state.regis.registered,
    username: state.regis.username,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (
      station,
      username,
      email,
      password,
      password2,
      livelink,
      image1,
      image2
    ) =>
      dispatch(
        actions.register(
          station,
          username,
          email,
          password,
          password2,
          livelink,
          image1,
          image2
        )
      ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);

import React from "react";
import { Route, Redirect } from "react-router-dom";
const ProtectedRoute = ({
  component: Component,
  user,
  station,
  suc,
  id,
  back,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (user || station == "True") {
          return <Component {...props} />;
        } else if (suc) {
          return <Component {...props} />;
        } else if (suc == false) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
};

export default ProtectedRoute;

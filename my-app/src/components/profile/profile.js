import React, { Component } from "react";
import BackDrop from "../backDrop/backdrop";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Aux from "../../hoc/Aux";
import ProfileUpdate from "./profile_update";
// import BackDrop from './backdrop'
import { NavLink } from "react-router-dom";
import * as actions from "../../store/actions/actions";
class profile extends Component {
  state = {
    show: false,
    profile_fire: false,
    livelink: null,
    update: false,
    edit: false,

    image: null,
    owner: null,
    livelink1: null,
    counter1: null,
    nepal1: null,
  };
  openBackdropHandler = () => {
    this.setState({ show: true, edit: true });
  };
  closeBackDrop = () => {
    this.setState({ show: false });
  };
  jpt = () => {
    setTimeout(() => {
      this.setState({
        owner: this.props.profile.profile.owner,
        livelink1: this.props.profile.profile.livelink,
        counter1: this.props.profile.profile.counter,
      });
    }, 1000);
  };
  profileupdate = () => {
    setTimeout(() => {
      this.setState({
        owner: this.props.profile_update.owner,
        livelink1: this.props.profile_update.livelink,
        counter1: this.props.profile_update.counter,
      });
    }, 1000);
  };
  componentDidMount() {
    var profile_id = null;

    if (this.props.isStation == "True") {
      profile_id = this.props.loginId;
    }

    this.props.onProfile(profile_id);
    this.jpt();
    console.log("profile id is ", this.props.profile_image);
    console.log("update is =====================", this.props.suc);
    this.setState({ update: true });
  }

  eventhandler = (data) => {
    this.setState({ show: false });
    console.log("data is ", data);
  };
  fileChangeHandler = (event) => {
    this.setState({ image: event.target.files[0] });
  };
  livelinkChangeHandler = (event) => {
    var name = event.target.name;
    var value = event.target.value;
    this.setState({
      livelink1: value,
      // file:event.target.files
    });
  };
  profileHandler = (event) => {
    event.preventDefault();

    var profile_id = null;
    this.props.onProfileUpdate(
      this.state.image,
      this.state.livelink1,
      this.state.owner,
      this.state.counter1
    );
    // this.setState({show:false})
    // this.profileupdate()
    console.log("edit is ", this.props.suc);
  };
  render() {
    console.log("profile is +++++++++++++", this.props, profile);
    
    return (
      <Aux>
        {(() => {
          switch (true) {
            case !this.props.suc && !this.state.show && this.props.image_status:
              return (
                <div className="content-wrapper">
                  <section className="content">
                    {/* CASE 1 */}
                    <div className="container-fluid">
                      <div className="row">
                        <div className="card mb-3">
                          <img
                            src={
                              process.env.PUBLIC_URL +
                              "/" +
                              this.props.profile.profile.image
                            }
                            style={{ maxHeight: "300px" }}
                          />
                          <div className="card-body">
                            <h1>case 1</h1>
                            <h1 className="card-title">
                              {this.props.profile.profile.livelink}
                            </h1>
                            <hr></hr>
                            <p className="card-text">
                              {this.props.profile.profile.counter} Subscribers
                            </p>
                            <hr></hr>
                            <button
                              type="button"
                              class="btn btn-outline-info"
                              onClick={this.openBackdropHandler}
                            >
                              Updata Profile
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              );
            case this.state.show && !this.props.suc:
              return (
                <div className="content-wrapper">
                  <section className="content">
                    <div className="container-fluid">
                      <div className="row">
                        <div
                          className="card mb-3"
                          style={{ maxWidth: "500px" }}
                        >
                          <img
                            src={
                              process.env.PUBLIC_URL +
                              "/" +
                              this.props.profile.profile.image
                            }
                            style={{ maxHeight: "300px" }}
                          />
                          <div className="card-body">
                            <h1>case 2</h1>
                            <h1 className="card-title">
                              {this.props.profile.profile.livelink}
                            </h1>
                            <hr></hr>
                            <p className="card-text">
                              {this.props.profile.profile.counter} Subscribers
                            </p>
                            <hr></hr>
                            <button
                              type="button"
                              class="btn btn-outline-info"
                              disabled={true}
                              onClick={this.openBackdropHandler}
                            >
                              Updata Profile
                            </button>
                          </div>
                        </div>
                        <div
                          className="card mb-3"
                          style={{
                            maxWidth: "500px",
                            marginLeft: "10px",
                            padding: "20px",
                          }}
                        >
                          <form>
                            <div className="form-group row">
                              <label
                                htmlFor="inputEmail3"
                                className="col-sm-2 col-form-label"
                              >
                                Title
                              </label>
                              <div className="col-sm-10">
                                <input
                                  type="text"
                                  className="form-control"
                                  id="inputEmail3"
                                  value={this.state.livelink1}
                                  onChange={this.livelinkChangeHandler}
                                />
                              </div>
                            </div>
                            <input
                              type="hidden"
                              name="owner"
                              value={this.state.owner}
                            />
                            <input
                              type="hidden"
                              name="counter"
                              value={this.state.counter1}
                            />

                            <div className="form-group row">
                              <label
                                htmlFor="inputPassword3"
                                className="col-sm-2 col-form-label"
                              >
                                Image
                              </label>
                              <div className="col-sm-10">
                                <input
                                  type="file"
                                  name="image"
                                  className="form-control"
                                  onChange={this.fileChangeHandler}
                                />
                              </div>
                            </div>

                            <div className="form-group row">
                              <div className="col-sm-3">
                                <button
                                  type="button"
                                  class="btn btn-outline-info"
                                  onClick={(event) => {
                                    this.profileHandler(event);
                                    this.eventhandler(event);
                                  }}
                                >
                                  Updata Profile
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              );
            case this.props.suc && !this.state.show:
              return (
                <div className="content-wrapper">
                  <section className="content">
                    {/* <img src={process.env.PUBLIC_URL + this.props.profile_update.image.replace("/my-app/public","")}/> */}

                    <div className="container-fluid">
                      <div className="row">
                        <div className="card mb-3">
                          <img
                            src={
                              process.env.PUBLIC_URL +
                              this.props.profile_update.image.replace(
                                "/my-app/public",
                                ""
                              )
                            }
                            alt="case 3"
                            style={{ maxHeight: "300px" }}
                          />
                          <div className="card-body">
                            <h1>case 3</h1>
                            <h1 className="card-title">
                              {this.props.profile_update.livelink}
                            </h1>
                            <hr></hr>
                            <p className="card-text">
                              {this.props.profile_update.counter} Subscribers
                            </p>
                            <hr></hr>
                            <button
                              type="button"
                              class="btn btn-outline-info"
                              onClick={this.openBackdropHandler}
                            >
                              Updata Profile
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              );
            case this.state.show && this.props.suc:
              return (
                <div className="content-wrapper">
                  <section className="content">
                    <div className="container-fluid">
                      <div className="row">
                        <div
                          className="card mb-3"
                          style={{ maxWidth: "500px" }}
                        >
                          <img
                            src={
                              process.env.PUBLIC_URL +
                              this.props.profile_update.image.replace(
                                "/my-app/public",
                                ""
                              )
                            }
                            style={{ maxHeight: "300px" }}
                          />
                          <div className="card-body">
                            <h1>case 4</h1>
                            <h1 className="card-title">
                              {this.props.profile_update.livelink}
                            </h1>
                            <hr></hr>
                            <p className="card-text">
                              {this.props.profile_update.counter} Subscribers
                            </p>
                            <hr></hr>
                            <button
                              type="button"
                              class="btn btn-outline-info"
                              disabled={true}
                              onClick={this.openBackdropHandler}
                            >
                              Updata Profile
                            </button>
                          </div>
                        </div>
                        <div
                          className="card mb-3"
                          style={{
                            maxWidth: "500px",
                            marginLeft: "10px",
                            padding: "20px",
                          }}
                        >
                          <form>
                            <div className="form-group row">
                              <label
                                htmlFor="inputEmail3"
                                className="col-sm-2 col-form-label"
                              >
                                Title
                              </label>
                              <div className="col-sm-10">
                                <input
                                  type="text"
                                  className="form-control"
                                  id="inputEmail3"
                                  value={this.state.livelink1}
                                  onChange={this.livelinkChangeHandler}
                                />
                              </div>
                            </div>
                            <input
                              type="hidden"
                              name="owner"
                              value={this.state.owner}
                            />
                            <input
                              type="hidden"
                              name="counter"
                              value={this.state.counter1}
                            />

                            <div className="form-group row">
                              <label
                                htmlFor="inputPassword3"
                                className="col-sm-2 col-form-label"
                              >
                                Image
                              </label>
                              <div className="col-sm-10">
                                <input
                                  type="file"
                                  name="image"
                                  className="form-control"
                                  onChange={this.fileChangeHandler}
                                />
                              </div>
                            </div>

                            <div className="form-group row">
                              <div className="col-sm-3">
                                <button
                                  type="button"
                                  class="btn btn-outline-info"
                                  onClick={(event) => {
                                    this.profileHandler(event);
                                    this.eventhandler(event);
                                  }}
                                >
                                  Updata Profile
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              );
            default:
              return null;
          }
        })()}
      </Aux>
    );
  }
}
var mapStateToProps = (state) => {
  return {
    loginId: state.log.userId,
    isAuthenticated: state.log.login,
    isStation: state.log.station,

    suc: state.update.suc,
    profile_update: state.update.success,
    profile: state.prof,
    success: state.update.success,
    image_status: state.prof.footer,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    onProfileUpdate: (image, livelink, owner, counter) =>
      dispatch(actions.updateProfile(image, livelink, owner, counter)),
    onProfile: (profile_id) => dispatch(actions.profile(profile_id)),
    // onIpSubmit:(ip,location,listen_date,station)=>dispatch(actions.ipSubmit(ip,location,listen_date,station)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(profile);

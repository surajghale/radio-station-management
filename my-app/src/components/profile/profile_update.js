import React, { Component } from "react";
import { connect } from "react-redux";
// import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBInput } from 'mdbreact';
// import { Button } from 'rsuite';
// import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";
import * as actions from "../../store/actions/actions";
import axios from "axios";
import Aux from "../../hoc/Aux";
import "./profile.css";
class AddSchedule extends Component {
  state = {
    image: this.props.profile.profile.image,
    owner: this.props.profile.profile.id,
    livelink: this.props.profile.profile.livelink,
    counter: this.props.profile.profile.counter,
    nepal: null,
    edit: false,
  };
  componentDidMount() {
    // const schedule_id_to_update  = this.props.match.params
    if (this.props.suc) {
      this.setState({
        owner: this.props.success.owner,
        livelink: this.props.success.livelink,
        counter: this.props.success.counter,
      });
    }
  }
  fileChangeHandler = (event) => {
    this.setState({ image: event.target.files[0] });
  };
  livelinkChangeHandler = (event) => {
    var name = event.target.name;
    var value = event.target.value;
    this.setState({
      livelink: value,
      // file:event.target.files
    });
  };
  profileHandler = (event) => {
    event.preventDefault();
    this.setState({ nepal: false });
    var profile_id = null;

    this.props.onProfileUpdate(
      this.state.image,
      this.state.livelink,
      this.state.owner,
      this.state.counter
    );
    console.log("edit is ", this.props.suc);
    // if(this.props.edits){
    //   setTimeout(()=>{

    //     this.props.onProfile(profile_id)
    //     if (this.props.onClick) {
    //     this.props.onClick(this.state);
    //     }
    //   },100)
    // }

    // if (this.props.onClick) {
    //   this.props.onClick(this.state);
    // }
    // this.props.history.push('/home')
  };
  render() {
    console.log("time  is", this.props.success);
    return (
      <div className="top">
        <div>
          <form>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">
                Title
              </label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputEmail3" />
              </div>
            </div>
            <div className="form-group row">
              <label
                htmlFor="inputPassword3"
                className="col-sm-2 col-form-label"
              >
                Image
              </label>
              <div className="col-sm-10">
                <input type="file" className="form-control" />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-sm-3">
                <button type="button" class="btn btn-outline-info">
                  Updata Profile
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
var mapStateToProps = (state) => {
  return {
    // scheduletoUpdate:state.sch.schedule,
    profile: state.prof,
    suc: state.update.suc,
    success: state.update.success,
  };
};
var mapDispatchToProps = (dispatch) => {
  return {
    // onUpdateSchedule:(title,dates,rj,image1,image2,owner,update_schedule_id)=>dispatch(actions.updatescheduleList(title,dates,rj,image1,image2,owner,update_schedule_id)),

    onProfileUpdate: (image, livelink, owner, counter) =>
      dispatch(actions.updateProfile(image, livelink, owner, counter)),
    onProfile: (profile_id) => dispatch(actions.profile(profile_id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddSchedule);

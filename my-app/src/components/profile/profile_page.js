import React, { Component } from 'react'
import Header from '../header'
import Sidebar from '../sidebar'
import ProfileBody from './profile'
export default class profile_page extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Sidebar/>
                <ProfileBody/>
            </div>
        )
    }
}

import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../store/actions/actions";
class Logout extends Component {
  componentDidMount() {
    this.props.onLogout();
  }
  render() {
    console.log("logout is ", this.props.logout);
    return <Redirect to="/" />;
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
    onLogout: () => dispatch(actions.logout()),
  };
};
var mapStateToProps = (state) => {
  return {
    logout: state.logout.islogout,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Logout);

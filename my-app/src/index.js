import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import "@fortawesome/fontawesome-free/css/all.min.css";
// import "bootstrap-css-only/css/bootstrap.min.css";
// import 'bootstrap/dist/css/bootstrap.css'
// import "mdbreact/dist/css/mdb.css";
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore ,applyMiddleware, compose,combineReducers} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './store/reducers/registerBuilder'
import login from './store/reducers/loginBuilder'
import schedule_show from './store/reducers/showSchedule'
// import Comment from './store/reducers/show_comment'
import Station from './store/reducers/showStationList'
import Profile from './store/reducers/show_profile'
// import Ip from './store/reducers/save_ip'
import profileUpdate from './store/reducers/profileUpdate';
import AddedSchedule from './store/reducers/add_schedule';
// import UpdateSchedule from './store/reducers/update_schedule';
// import DeleteSchedule from './store/reducers/delete_schedule';
import RjDetail from './store/reducers/rj_detail';
// import DashSchedule from './components/dashboard4/add_schedule';
// import { RJ_DETAIL } from './store/actions/actionTypes';
// import RjUpdate from './store/reducers/rj_update_schedule'
import Logoutdetails from './store/reducers/logout'
import AllSchedule from './store/reducers/all_schedule'
import SingleSchedule from './store/reducers/single_schedule'
import SingleRequest from './store/reducers/single_request'
import RjUpdate from './store/reducers/rj_update_schedule'
import RjUpdateRequest from './store/reducers/rj_update_request'
import ShowRjRequestList from './store/reducers/show_request_list'
import SubscriberDetail from './store/reducers/subscriber_counter'
import SubscriberDelete from './store/reducers/delete_sub'
import SaveIp from './store/reducers/save_ip'

// import 'bootstrap/dist/css/bootstrap.min.css';
// import Dates from './components/profile'
// import 'bootstrap-css-only/css/bootstrap.min.css';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const reducer = combineReducers({
    regis:rootReducer,
    log:login,
    logout:Logoutdetails,
    sch:schedule_show,
    ip:SaveIp,
    // cmnts:Comment,
    stations:Station,
    prof:Profile,
    update:profileUpdate,
    added_schedule:AddedSchedule,
    // updated_schedule:UpdateSchedule,
    // delete_schedule:DeleteSchedule, 
    rj_details:RjDetail,
    all:AllSchedule,
    single:SingleSchedule,
    rj_update:RjUpdate,
    rj_request:RjUpdateRequest,
    reqeust_list_rj:ShowRjRequestList,
    singles:SingleRequest,
    subscriber:SubscriberDetail,
    after_delete_sub_counter:SubscriberDelete
    // rj_update:RjUpdate,
    
})
const store = createStore(reducer,composeEnhancers(applyMiddleware(thunk)))
const route =(
    <Provider store ={store}>
        <BrowserRouter>
    <App/>
    </BrowserRouter>
    </Provider>
)
ReactDOM.render(route, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

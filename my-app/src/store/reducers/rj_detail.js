import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  rj: null,
  rj_status: false,
};
const ipDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.RJ_DETAIL:
      return {
        ...state,
        rj: action.data,
        rj_status: true,
      };
    default:
      return state;
  }
};
export default ipDetails;

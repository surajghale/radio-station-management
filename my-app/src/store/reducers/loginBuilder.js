import * as actionTypes from "../actions/actionTypes";
var initialState = {
  login: false,
  username: null,
  loading: false,
  userId: null,
  station: null,
};
var login = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        username: action.username,
        loading: false,
        login: true,
        userId: action.id,
        station: action.is_station,
      };
    case actionTypes.LOGOUT:
      return {
        ...state,
        username: null,
        login: false,
        loading: false,
        userId: null,
      };

    default:
      return state;
  }
};
export default login;

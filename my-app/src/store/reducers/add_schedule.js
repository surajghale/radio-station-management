import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  // userDetail:null,
  added: false,
};
const addSchedule = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ADD_SCHEDULE:
      return {
        ...state,
        //    userDetail:action.ips,
        added: true,
      };
    default:
      return state;
  }
};
export default addSchedule;

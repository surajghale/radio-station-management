import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  rj: null,
  rj_image: null,
  rj_status: false,
  image_status: false,
};
const ipDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.UPDATE_RJ:
      return {
        ...state,
        rj: action.rj_update,
        //    rj_image:action.rj_update_image,
        rj_status: true,
      };
    //    case actionType.UPDATE_RJ_IMAGE:
    //     return {
    //         ...state,
    //         // rj:action.rj_update,
    //         rj_image:action.rj_update_image,
    //         // rj_status:true,
    //         image_status:true
    //     }
    default:
      return state;
  }
};
export default ipDetails;

import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  islogout: false,
};
const logoutDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.LOGOUT:
      return {
        ...state,
        islogout: true,
        //    dates:action.dates
      };
    default:
      return state;
  }
};
export default logoutDetails;

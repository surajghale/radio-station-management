import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  userDetail: null,
  dates: null,
};
const ipDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SHOW_IP:
      return {
        ...state,
        userDetail: action.ips,
        dates: action.dates,
      };
    default:
      return state;
  }
};
export default ipDetails;

import * as actionTypes from "../actions/actionTypes";
const initialState = {
  station: [],
  edit: false,
  footer: false,
};
const stationlist = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_STATION:
      return {
        ...state,
        station: action.station,
        edit: true,
      };
    default:
      return state;
  }
};
export default stationlist;

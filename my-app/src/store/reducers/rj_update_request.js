import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  rjRequest: null,
  rj_request_status: false,
};
const requestupdate = (state = initialState, action) => {
  switch (action.type) {
    case actionType.UPDATE_RJ_REQUEST:
      return {
        ...state,
        rjRequest: action.rj_update_request,
        rj_request_status: true,
      };
    default:
      return state;
  }
};
export default requestupdate;

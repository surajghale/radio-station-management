import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  success: null,
  suc: false,
};
const profileUpdate = (state = initialState, action) => {
  switch (action.type) {
    case actionType.UPDATE:
      return {
        ...state,
        success: action.success,
        suc: true,
      };
    default:
      return state;
  }
};
export default profileUpdate;

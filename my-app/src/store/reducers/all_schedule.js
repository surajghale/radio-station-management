import * as actionTypes from "../actions/actionTypes";
const initialState = {
  schedule: [],
  edit: false,
};
const all = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ALL_SCHEDULE:
      return {
        ...state,
        schedule: action.schedule,
        edit: true,
      };
    default:
      return state;
  }
};
export default all;

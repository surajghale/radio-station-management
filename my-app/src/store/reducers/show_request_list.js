import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  show_list: [],
  show_list_status: false,
};
const ipDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SHOW_REQUEST:
      return {
        ...state,
        show_list: action.list,
        show_list_status: true,
      };
    default:
      return state;
  }
};
export default ipDetails;

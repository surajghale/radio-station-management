import * as actionTypes from "../actions/actionTypes";
const initialState = {
  username: null,
  loading: false,
  registered: false,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.REGISTRATION_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.REGISTRATION_SUCCESS:
      return {
        ...state,
        username: action.username,
        registered: true,
      };
    case actionTypes.REGISTRATION_FAILED:
      return {
        ...state,
        username: action.username,
        registered: false,
      };
    default:
      return state;
  }
};
export default reducer;

import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  update_schedule: null,
  update: false,
};
const ipDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.UPDATE_SCHEDULE:
      return {
        ...state,
        update_schedule: action.update,
        update: true,
      };
    default:
      return state;
  }
};
export default ipDetails;

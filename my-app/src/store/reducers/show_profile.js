import * as actionTypes from "../actions/actionTypes";
const initialState = {
  profile: null,
  footer: false,
};
const profile_data = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_PROFILE:
      return {
        ...state,
        profile: action.profile,
        //   link:action.link,
        footer: true,
      };
    default:
      return state;
  }
};
export default profile_data;

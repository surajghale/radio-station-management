import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  single_schedule_data: null,
  // dates:null
};
const single = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SINGLE_SCHEDULE:
      return {
        ...state,
        single_schedule_data: action.success,
        //    dates:action.dates
      };
    default:
      return state;
  }
};
export default single;

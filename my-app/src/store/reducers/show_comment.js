import * as actionTypes from "../actions/actionTypes";
const initialState = {
  comment: [],
  edit: false,
};
const commentlist = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_COMMENT:
      return {
        ...state,
        comment: action.comment,
        edit: true,
      };

    default:
      return state;
  }
};
export default commentlist;

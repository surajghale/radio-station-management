import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  single_request_data: null,
  // dates:null
};
const single = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SINGLE_REQUEST:
      return {
        ...state,
        single_request_data: action.success,
        //    dates:action.dates
      };
    default:
      return state;
  }
};
export default single;

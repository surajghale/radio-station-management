import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  delete_data: null,
  delete: false,
};
const deleteSchedule = (state = initialState, action) => {
  switch (action.type) {
    case actionType.DELETE_SCHEDULE:
      return {
        ...state,
        delete_data: action.data,
        delete: true,
      };
    default:
      return state;
  }
};
export default deleteSchedule;

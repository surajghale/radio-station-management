import react ,{ Component } from "react"
import * as actionType from '../actions/actionTypes'
const initialState={
    after_sub_delete:null,
    delete_status:false
}
const ipDetails =(state = initialState,action)=>{
       switch(action.type){
           case actionType.SUBSCRIBER_DELETE:
               return {
                   ...state,
                   userDetail:action.subscriber_count,
                   delete_status:true
               }
               default:
                return state
       }
}
export default ipDetails
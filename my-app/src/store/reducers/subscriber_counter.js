import react, { Component } from "react";
import * as actionType from "../actions/actionTypes";
const initialState = {
  sub_counter: null,
  clicked: false,
};
const ipDetails = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SUBSCRIBER_COUNTER:
      return {
        ...state,
        sub_counter: action.subscriber_count,
        clicked: true,
      };
    default:
      return state;
  }
};
export default ipDetails;

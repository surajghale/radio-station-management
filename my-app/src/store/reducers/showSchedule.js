import * as actionTypes from "../actions/actionTypes";
const initialState = {
  schedule: [],
  edit: false,
};
const schedulelist = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_SCHEDULE:
      return {
        ...state,
        schedule: action.schedule,
        edit: true,
      };
    default:
      return state;
  }
};
export default schedulelist;

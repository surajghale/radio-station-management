import * as actionTypes from './actionTypes';
import axios from 'axios';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';

// import { compose } from 'recompose'
export const regsitration_start=()=>{
    return {
        type:actionTypes.REGISTRATION_START,
    }
}
export const rgistration_success=(username)=>{
    return {
        type:actionTypes.REGISTRATION_SUCCESS,
        username:username
    }
}
export const rgistration_failed=(username)=>{
    return {
        type:actionTypes.REGISTRATION_FAILED,
        username:username
    }
}
export const register =(station,username,email,password,password2,livelink,image1,image2)=>{
    return dispatch=>{
        dispatch(regsitration_start());
        var formData = new FormData();
    formData.append('is_station',station);

    formData.append('username',username);
    formData.append('email',email);
    formData.append('password',password);
    formData.append('password2',password2);
    formData.append('livelink',livelink);
    formData.append('image',image1,image2);
    
    // var data ={
    //   name:this.state.formdata.name.value,
    //   email:this.state.formdata.email.value,
    //   password:this.state.formdata.password.value,
    //   password2:this.state.formdata.password2.value,
    //   livelink:this.state.formdata.livelink.value,
    //   file:this.state.file[0]
    // }
    console.log(formData)
    axios.post('http://127.0.0.1:8000/account/api/register',formData,{
      headers: {
        'content-type': 'multipart/form-data'
      }
    }).then(Response=>{
      console.log('response data is ',Response.data)
      var lens = Object.keys(Response.data.username[0]).length
      console.log('lenght data is ',lens)
      console.log('success')
      if(lens>1){
        dispatch(rgistration_success(Response.data.username))
      }else{
        dispatch(rgistration_failed(Response.data.username))
        console.log("failed data is ========",Response.data)

      }
    // this.props.history.push('/login')
    }).catch(error=>{
        console.log('failed')
    })
    }
}

//login actions
export const login_start=()=>{
    return{
        type:actionTypes.LOGIN_START
    }
}
export const login_success=(username,id,is_station)=>{
    return {
        type:actionTypes.LOGIN_SUCCESS,
        username:username,
        id:id,
        is_station:is_station,
    }
}
export const show_schedule=(schedule)=>{
    return {
        type:actionTypes.SHOW_SCHEDULE,
        schedule:schedule,
    }
}
export const show_station=(station)=>{
    return {
        type:actionTypes.SHOW_STATION,
        station:station,
    }
}
export const show_comment=(comment)=>{
    return {
        type:actionTypes.SHOW_COMMENT,
        comment:comment,
    }
}
export const update_success=(success)=>{
    return {
        type:actionTypes.UPDATE,
        success:success,
    }
}
export const show_ip=(ips,dates)=>{
    return {
        type:actionTypes.SHOW_IP,
        ips:ips,
        dates:dates
    }
}
export const show_profile=(profile)=>{
    return {
        type:actionTypes.SHOW_PROFILE,
        profile:profile
    }
}
export const add_schedule=(data)=>{
    return {
        type:actionTypes.ADD_SCHEDULE,
        // profile:profile
    }
}
export const showRequested=(data)=>{
    return {
        type:actionTypes.SHOW_REQUEST,
        list:data
        // profile:profile
    }
}

export const all_schedule=(data)=>{
    return {
        type:actionTypes.ALL_SCHEDULE,
        schedule:data
    }
}
export const single_success=(success)=>{
    return {
        type:actionTypes.SINGLE_SCHEDULE,
        success:success,
    }
}
export const single_request=(success)=>{
    return {
        type:actionTypes.SINGLE_REQUEST,
        success:success,
    }
}
export const update_schedule=(update)=>{
    return {
        type:actionTypes.UPDATE_SCHEDULE,
        update:update
    }
}
export const update_rj=(update)=>{
    return {
        type:actionTypes.UPDATE_RJ,
        rj_update:update
    }
}
export const update_rj_image=(update,bol)=>{
    return {
        type:actionTypes.UPDATE_RJ_IMAGE,
        rj_update_image:update,
        
    }
}
export const update_rj_request=(update)=>{
    return {
        type:actionTypes.UPDATE_RJ_REQUEST,
        rj_update_request:update,
        
    }
}

export const delete_success=(data)=>{
    return {
        type:actionTypes.DELETE_SCHEDULE,
        data:data
    }
}
export const show_rj=(data)=>{
    return {
        type:actionTypes.RJ_DETAIL,
        data:data
    }
}
export const subscriber=(data)=>{
    return {
        type:actionTypes.SUBSCRIBER_COUNTER,
        subscriber_count:data
    }
}
export const delete_subscriber=(data)=>{
    return {
        type:actionTypes.SUBSCRIBER_DELETE,
        subscriber_count:data
    }
}

export const logout=()=>{
    localStorage.removeItem('token')
    localStorage.removeItem('id')
    localStorage.removeItem('is_station')
    return{
        type:actionTypes.LOGOUT
    }
}
export const login=(email,password)=>{
    return dispatch=>{
        dispatch(login_start())
        var formData = new FormData();
        formData.append('email',email)
        formData.append('password',password)
        // var data ={
        //     email:email,
        //     password:password
        // }
        var headers = {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*"
            
        }
        console.log('data is ',formData)
        axios.post('http://127.0.0.1:8000/account/api/token-auth',formData).then(Response=>{
        console.log('response data is ',Response.data)
        var jpt =Response.data.user.username
        var stationUpper =Response.data.user.is_station
        var sUpper = stationUpper.charAt(0).toUpperCase()+stationUpper.slice(1)

        localStorage.setItem('token',Response.data.token)
        localStorage.setItem('id',Response.data.user.id)
        localStorage.setItem('is_station',sUpper)
        localStorage.setItem('is_rj',Response.data.user.is_rj)
        console.log('success')
        if(jpt){
            console.log('wow')
        }
        else{
            console.log('sad')
        }
        dispatch(login_success(Response.data.user.username,Response.data.user.id,sUpper))
        // dispatch(login_success(Response.data.username))
        //  this.props.history.push('/')
      }).catch(error=>{
          console.log('failed')
      })
    }
}
export const scheduleSubmit=(title,dates,rj,image1,image2,owner)=>{
return dispatch=>{
    var formData = new FormData();
    var da ="2020-01-29 18:36:12"
    var start = dates[0]
    var end = dates[1]
    var standardEndFormat = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`

    var standardStartFormat = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`
    // var getfullyear = start
    console.log("date and time is",standardStartFormat,standardEndFormat)
    var m_image = image1
    console.log('image length is ',m_image[0])
    // var mulimage = []
    //     var jpt = image1
    //     jpt.map(el=>{
    //       mulimage.push(el.name)
    //     })
    //     // var joinedimage = mulimage.join('-')
    //     // console.log('element is ',joinedimage)
    //     // var new_image = {0:joinedimage}
    //     var data = new Blob([mulimage],{type: 'image/png'})
    //     var datas = new File([data],[mulimage],data)
    //     console.log("data is ",datas)
    //     console.log("data is ",data)
    // console.log(typeof dates,time[0])
    var data = [{0:m_image[0],1:m_image[1]}]
    formData.append('title',title)
    formData.append('datetime_field_start',standardStartFormat)
    formData.append('datetime_field_end',standardEndFormat)
       
    formData.append('rj',rj)
    if(m_image.length==1){
        formData.append('image',m_image[0])
    }
    else if(m_image.length==2){
        formData.append('image',m_image[0])
        formData.append('image',m_image[1])

        
    }
    // formData.append('image',image1)
    formData.append('owner',owner)
    // formData.append('timefield',time)
    console.log('the real rj is ',rj)
    console.log('form data is ',formData)
    axios.post('http://127.0.0.1:8000/account/api/view_schedule',formData).then(res=>{
       console.log(res.data)
        dispatch(add_schedule(res.data))
    }).catch(err=>{
        console.log('error message')
    })

}
}
export const ipSubmit=(ip,location,start,station)=>{
    return dispatch=>{
        var formData = new FormData();
        // var start =dates
        // console.log("ip date is ",da)
        var dap = "2020-4-6 23:21"
        // var start = dates[0]
        // var end = dates[1]
        // var standardEndFormat = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`
        var standardStartFormat = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`
        // console.log("date and time is",standardStartFormat,standardEndFormat)
        // var m_image = image1
        // console.log('image length is ',m_image[0])
        formData.append('ip',ip)
        formData.append('location',location)
        formData.append('listen_date',standardStartFormat)
        formData.append('station',station)
        axios.post('http://127.0.0.1:8000/account/api/ip_track/',formData).then(res=>{
           console.log("ip details are",res.data)
        }).catch(err=>{
            console.log('error message')
        })
    
    }
}
export const subscribeSubmit=(profile)=>{
    return dispatch=>{
        var formData = new FormData();
        var acc = localStorage.getItem('id')
        console.log("profile is $$$$$$$$$$$$$$",profile)
        formData.append('profile',profile)
        formData.append('account',acc)
        axios.post('http://127.0.0.1:8000/account/api/subscribed_user/',formData).then(res=>{
           console.log("subscribe : ",res.data)
           dispatch(subscriber(res.data))
        }).catch(err=>{
            console.log('error message')
        })
    
    }
    }
export const singleSchedule=(id)=>{
        return dispatch=>{
            // localStorage.setItem('datas',0)
            // var id = id
            // var prof_id = localStorage.getItem('id')
                axios.get(`http://127.0.0.1:8000/account/api/single_schedule/${id}`).then(res=>{
                  console.log('single schedule data ',res.data)
                  dispatch(single_success(res.data))
                //   this.setState({delete:true})
              }).catch(err=>{
                console.log(err)
            })
        }
    }
    export const singleRequest=(id)=>{
        return dispatch=>{
            // localStorage.setItem('datas',0)
            // var id = id
            // var prof_id = localStorage.getItem('id')
                axios.get(`http://127.0.0.1:8000/account/api/single_request/${id}`).then(res=>{
                  console.log('single schedule data ',res.data)
                  dispatch(single_request(res.data))
                //   this.setState({delete:true})
              }).catch(err=>{
                console.log(err)
            })
        }
    }
// export const singleSchedule=(id)=>{
//         return dispatch=>{
//             // localStorage.setItem('datas',0)
//             // var id = id
//             // var prof_id = localStorage.getItem('id')
//                 axios.get(`http://127.0.0.1:8000/account/api/single_schedule/${id}`).then(res=>{
//                   console.log('single schedule data ',res.data)
//                   dispatch(single_success(res.data))
//                 //   this.setState({delete:true})
//               }).catch(err=>{
//                 console.log(err)
//             })
//         }
//     }
export const scheduleList=()=>{
    return dispatch=>{
        var id = localStorage.getItem('id')
        var nepal=null
        axios.get(`http://127.0.0.1:8000/account/api/show_schedule/${id}`).then(res=>{
            console.log("schedule list are",res.data)
            nepal = res.data
            dispatch(show_schedule(res.data.schedule))
        }).catch(err=>{
            console.log(err)
        })
        axios.get(`http://127.0.0.1:8000/account/api/image/`).then(res=>{
            console.log("rj details  list are===================",res.data)
            dispatch(show_rj(res.data.Rj_Detail))
        }).catch(err=>{
            console.log(err)
        })
        console.log("nepal is !!!!!!!!!!!!!!!!!!!",nepal)
    }
}
export const all_schedules=()=>{
    return dispatch=>{
        // var id = localStorage.getItem('id')
        axios.get(`http://127.0.0.1:8000/account/api/all_schedule`).then(res=>{
            console.log(res.data)
            dispatch(all_schedule(res.data.schedule))
        }).catch(err=>{
            console.log(err)
        })
    }
}
export const stationList=()=>{
    return dispatch=>{
        // var id = localStorage.getItem('id')
        axios.get(`http://127.0.0.1:8000/account/api/profile_list/`).then(res=>{
            console.log(res.data.station[0].title)
            dispatch(show_station(res.data.station))
        }).catch(err=>{
            console.log(err)
        })
    }
}
export const commentList=()=>{
    return dispatch=>{
        var id = localStorage.getItem('id')
        axios.get(`http://127.0.0.1:8000/account/api/show_comment/${id}`).then(res=>{
            console.log(res.data.schedule)
            dispatch(show_comment(res.data.schedule))
        }).catch(err=>{
            console.log(err)
        })
    }
}

export const profile=(id)=>{
    return dispatch=>{
        // localStorage.setItem('datas',0)
        // var id = id
        var prof_id = localStorage.getItem('id')
        axios.get(`http://127.0.0.1:8000/account/api/profile/${id}`).then(res=>{
            console.log('profile data ',res.data)
            dispatch(show_profile(res.data))
          }).catch(err=>{
            console.log(err)
        })
    }
}
export const deleteSchedule=(id)=>{
    return dispatch=>{
        // localStorage.setItem('datas',0)
        // var id = id
        // var prof_id = localStorage.getItem('id')
            axios.delete(`http://127.0.0.1:8000/account/api/delete_schedule/${id}`).then(res=>{
              console.log('delete schedule data ',res.data)
              dispatch(delete_success(res.data))
            //   this.setState({delete:true})
          }).catch(err=>{
            console.log(err)
        })
    }
}
// UNSUBSCRIBE ACTION==========================

export const deleteSubscribe=(profile_id,log_id)=>{
    return dispatch=>{
        // localStorage.setItem('datas',0)
        // var id = id
        // var prof_id = localStorage.getItem('id')
            axios.delete(`http://127.0.0.1:8000/account/api/delete_subscribe/${profile_id}/${log_id}`).then(res=>{
              console.log('delete status data ',res.data)
            dispatch(delete_subscriber(res.data))
            //   this.setState({delete:true})
          }).catch(err=>{
            console.log(err)
        })
    }
}
// THIS FUNCTION IS FOR RUNNIG THE SERVER TO FIRE EVENT TO UPDATE THE SYSTEM AT EVERY MIDNIGHT 
export const servertime=()=>{
    return dispatch=>{
        // localStorage.setItem('datas',0)
        // var id = id
        axios.post(`http://127.0.0.1:8000/account/api/server`).then(res=>{
            console.log('server data ',res.data)
            // dispatch(show_profile(res.data))
          }).catch(err=>{
            console.log(err)
        })
    }
}
// export const newDate=()=>{
//     return dispatch=>{
//         var id = localStorage.getItem('id')
//         axios.get(`http://127.0.0.1:8000/account/api/show_schedule/${id}`).then(res=>{
//             console.log("profile schedule is ",res.data.schedule)
//             var date = res.data.schedule
//             dispatch(show_comment(res.data.schedule))

//         }).catch(err=>{
//             console.log(err)
//         })
//     }
// }
export const updatescheduleList=(title,dates,rj,image1,image2,owner,update_schedule_id)=>{
    return dispatch=>{
        var formData = new FormData();
        var da ="2020-01-29 18:36:12"
        var start = dates[0]
        var end = dates[1]
        var standardEndFormat = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`
        var standardStartFormat = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`
        // var getfullyear = start
        console.log("rj is -------------",rj)
        console.log("date and time is",standardStartFormat,standardEndFormat)
        var m_image = image1
        if(m_image.length==1){
            formData.append('image',m_image[0])
        }
        else if(m_image.length==2){
            formData.append('image',m_image[0])
            formData.append('image',m_image[1])
        }
        // console.log(typeof dates,time[0])
        formData.append('title',title)
        formData.append('datetime_field_start',standardStartFormat)
        formData.append('datetime_field_end',standardEndFormat)
        formData.append('rj',rj)
        // formData.append('image',image1,image2)
        formData.append('owner',owner)
        // formData.append('timefield',time)
        console.log('the real rj is ',rj)
        console.log('form data is ',formData)
        var jpt = update_schedule_id
        console.log('scheudle_update_id is ',jpt)
        // var schedule_to_update = props.match.params
        // console.log(schedule_to_update)
        var id = localStorage.getItem('id')
        axios.put(`http://127.0.0.1:8000/account/api/update_schedule/${jpt}`,formData).then(res=>{
            console.log("update schedule are",res.data)
            dispatch(update_schedule(res.data.updated_schedule))
            // dispatch(show_schedule(res.data.schedule))
            setTimeout(()=>{
                axios.get(`http://127.0.0.1:8000/account/api/image/`,formData).then(res=>{
            console.log("update image are=====================",res.data)
            // dispatch(update_schedule(res.data.updated_schedule))
            // dispatch(show_schedule(res.data.schedule))
            
        }).catch(err=>{
            console.log(err)
        })
            },1000)
            
        }).catch(err=>{
            console.log(err)
        })
    }
}
export const updateRj=(title,dates,rj,image1,image2,owner,update_schedule_id)=>{
    return dispatch=>{
        var formData = new FormData();
        var da ="2020-01-29 18:36:12"
        var start = dates[0]
        var end = dates[1]
        console.log("date is ====++++++++++",start,end)
        var standardEndFormat = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`
        var standardStartFormat = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`
        // var getfullyear = start
        console.log("date and time is",standardStartFormat,standardEndFormat)
        var m_image = image1
        if(m_image.length==1){
            formData.append('image',m_image[0])
        }
        else if(m_image.length==2){
            formData.append('image',m_image[0])
            formData.append('image',m_image[1])
        }
        // console.log(typeof dates,time[0])
        formData.append('title',title)
        formData.append('datetime_field_start',standardStartFormat)
        formData.append('datetime_field_end',standardEndFormat)
        formData.append('rj',rj)
        // formData.append('image',image1,image2)
        formData.append('owner',owner)
        // formData.append('timefield',time)
        console.log('the real rj is ',rj)
        console.log('form data is ',formData)
        var jpt = update_schedule_id
        console.log('scheudle_update_id is ',jpt)
        // var schedule_to_update = props.match.params
        // console.log(schedule_to_update)
        var id = localStorage.getItem('id')
        console.log("detail single +++++++++++++++++",title,standardStartFormat,standardEndFormat,rj,image1,owner,update_schedule_id)
        axios.put(`http://127.0.0.1:8000/account/api/update_rj/${jpt}`,formData).then(res=>{
            console.log("update schedule are",res.data)
            dispatch(update_rj(res.data))
            // dispatch(show_schedule(res.data.schedule))
        //     setTimeout(()=>{
        //         axios.get(`http://127.0.0.1:8000/account/api/image/`,formData).then(res=>{

        //     console.log("update image are=====================",res.data)
        //     // dispatch(update_schedule(res.data.updated_schedule))
        //     // dispatch(show_schedule(res.data.schedule))
        //     dispatch(update_rj_image(res.data.Rj_Detail))
        // }).catch(err=>{
        //     console.log(err)
        // })
        //     },500)
            
           
            
        }).catch(err=>{
            console.log(err)
        })
        
    }
}
// APPROVE RJ ================================


export const approveRj=(title,dates,rj,image1,image2,owner,update_schedule_id)=>{
    return dispatch=>{
        var formData = new FormData();
        var da ="2020-01-29 18:36:12"
        var start = dates[0]
        var end = dates[1]
        console.log("date is ====++++++++++",start,end)
        var standardEndFormat = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`
        var standardStartFormat = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`
        // var getfullyear = start
        console.log("date and time is",standardStartFormat,standardEndFormat)
        var m_image = image1
        if(m_image.length==1){
            formData.append('image',m_image[0])
        }
        else if(m_image.length==2){
            formData.append('image',m_image[0])
            formData.append('image',m_image[1])
        }
        // console.log(typeof dates,time[0])
        formData.append('title',title)
        formData.append('datetime_field_start',standardStartFormat)
        formData.append('datetime_field_end',standardEndFormat)
        formData.append('rj',rj)
        // formData.append('image',image1,image2)
        formData.append('owner',owner)
        // formData.append('timefield',time)
        console.log('the real rj is ',rj)
        console.log('form data is ',formData)
        var jpt = update_schedule_id
        console.log('scheudle_update_id is ',jpt)
        // var schedule_to_update = props.match.params
        // console.log(schedule_to_update)
        var id = localStorage.getItem('id')
        console.log("detail single +++++++++++++++++",title,standardStartFormat,standardEndFormat,rj,image1,owner,update_schedule_id)
        axios.put(`http://127.0.0.1:8000/account/api/approve_rj/${jpt}`,formData).then(res=>{
            console.log("update schedule are",res.data)
            // dispatch(update_rj(res.data))
            // dispatch(show_schedule(res.data.schedule))
        //     setTimeout(()=>{
        //         axios.get(`http://127.0.0.1:8000/account/api/image/`,formData).then(res=>{

        //     console.log("update image are=====================",res.data)
        //     // dispatch(update_schedule(res.data.updated_schedule))
        //     // dispatch(show_schedule(res.data.schedule))
        //     dispatch(update_rj_image(res.data.Rj_Detail))
        // }).catch(err=>{
        //     console.log(err)
        // })
        //     },500)
            
           
            
        }).catch(err=>{
            console.log(err)
        })
        
    }
}
//RJ UPDATE REQUESTT====================

export const RjUpdateRequest=(title,dates,rj,image1,image2,owner,update_schedule_id)=>{
    return dispatch=>{
        var formData = new FormData();
        var da ="2020-01-29 18:36:12"
        var start = dates[0]
        var end = dates[1]
        var standardEndFormat = `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()} ${end.getHours()}:${end.getMinutes()}`
        var standardStartFormat = `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()} ${start.getHours()}:${start.getMinutes()}`
        // var getfullyear = start
        console.log("date and time is",standardStartFormat,standardEndFormat)
        var m_image = image1
        if(m_image.length==1){
            formData.append('image',m_image[0])
        }
        else if(m_image.length==2){
            formData.append('image',m_image[0])
            formData.append('image',m_image[1])
        }
        // console.log(typeof dates,time[0])
        formData.append('title',title)
        formData.append('datetime_field_start',standardStartFormat)
        formData.append('datetime_field_end',standardEndFormat)
        formData.append('rj',rj)
        // formData.append('image',image1,image2)
        formData.append('owner',owner)
        // formData.append('timefield',time)
        console.log('the real rj is ',rj)
        console.log('form data is ',formData)
        var jpt = update_schedule_id
        console.log('scheudle_update_id is ',jpt)
        // var schedule_to_update = props.match.params
        // console.log(schedule_to_update)
        var id = localStorage.getItem('id')
        console.log("detail single +++++++++++++++++",title,standardStartFormat,standardEndFormat,rj,image1,owner,update_schedule_id)
        axios.post(`http://127.0.0.1:8000/account/api/update_rj_request/${id}`,formData).then(res=>{
            console.log("update request are",res.data)
            dispatch(update_rj_request(res.data))
            // dispatch(show_schedule(res.data.schedule))
        //     setTimeout(()=>{
        //         axios.get(`http://127.0.0.1:8000/account/api/image/`,formData).then(res=>{

        //     console.log("update image are=====================",res.data)
        //     // dispatch(update_schedule(res.data.updated_schedule))
        //     // dispatch(show_schedule(res.data.schedule))
        //     dispatch(update_rj_image(res.data.Rj_Detail))
        // }).catch(err=>{
        //     console.log(err)
        // })
        //     },500)
            
           
            
        }).catch(err=>{
            console.log(err)
        })
        
    }
}

//SHOW REQUEST FOR RJ UPDATE==================================
export const showRjRequest=()=>{
    return dispatch=>{
        
            axios.get(`http://127.0.0.1:8000/account/api/show_rj_request`).then(
                Response=>{
                    
                    console.log('show Rj REQUEST DATA========= ',Response.data)
                    dispatch(showRequested(Response.data.rj_update_request))
                    // dispatch(login_success(Response.data.user.username,Response.data.user.id))
    
                }
            )
    }
}

export const updateProfile=(image,livelink,owner,counter)=>{
    return dispatch=>{
        var formData = new FormData();
        
        formData.append('image',image)
        formData.append('livelink',livelink)
        formData.append('owner',owner)
        formData.append('counter',counter)
        
        var id = localStorage.getItem('id')
        axios.put(`http://127.0.0.1:8000/account/api/profile_update/${id}`,formData).then(res=>{
            console.log(res.data)
            dispatch(update_success(res.data.profile))
        }).catch(err=>{
            console.log(err)
        })
    }
}
export const hitcount = ()=>{
    return dispatch=>{
        // const DEFAULT_QUERY = 'redux';
        axios.get('http://ip-api.com/json').then(res=>res

).then(resp=>{
    console.log(resp.data['query'])
    var ip=resp.data['query']
    console.log("ip address is",resp.data)
    var dates = new Date()
    // var onUnload = e => { // the method that will be used for both add and remove event
    //     e.preventDefault();
    //     e.returnValue = '';
    //     var enddates=new Date()
    //     // this.setState({date:new Date})
    //  }
    //  window.addEventListener("beforeunload", this.onUnload);

    dispatch(show_ip(resp.data,dates))
})

    }
 
}
//THIS IS FOR CHECKING WHETHER THE USER IS STILL LOGGED TO MAKE STATE UNCHANGE IF TOKEN IS IN THE LOCAL STORAGE
export const signInCheck=()=>{
    return dispatch=>{
        
        var signinToken=localStorage.getItem('token')
        var ids = localStorage.getItem('id')
        // var stationUpper =Response.data.user.is_station
        var sUpper = localStorage.getItem('is_station')
        console.log('token is ',signinToken,ids,sUpper)
        if(signinToken){
            axios.get(`http://127.0.0.1:8000/account/api/current_user/${ids}`).then(
                Response=>{
                    
                    console.log('every time run data is ',Response.data)
                    dispatch(login_success(Response.data.username,Response.data.id,sUpper))
                    // dispatch(login_success(Response.data.user.username,Response.data.user.id))
    
                }
            )
        }
        else{
            console.log('failed')
        }
        
        
    }
    // var signinToken=localStorage.getItem('token')
    // console.log(signinToken)
}
// var mapStateToProps =state=>{
//     return{
//         val:
//     }
// }
// export const connect(mapStateToProps) (updatescheduleList)
// // export default withRouter( updatescheduleList )
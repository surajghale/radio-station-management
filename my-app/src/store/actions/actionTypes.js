export const REGISTRATION_START='REGISTRATION_START';
export const REGISTRATION_SUCCESS='REGISTRATION_SUCCESS';
export const REGISTRATION_FAILED='REGISTRATION_FAILED';
export const LOGIN_START='LOGIN_START';
export const LOGIN_SUCCESS='LOGIN_SUCCESS';
export const LOGIN_FAILED='LOGIN_FAILED';
export const LOGOUT='LOGOUT';
export const SCHEDULE='SCHEDULE';
export const SHOW_SCHEDULE="SHOW_SCHEDULE";
export const SHOW_IP = "SHOW_IP";
export const SHOW_COMMENT="SHOW_COMMENT";
export const SHOW_STATION="SHOW_STATION";
export const SHOW_PROFILE="SHOW_PROFILE";
export const UPDATE="UPDATE";
export const ADD_SCHEDULE="ADD_SCHEDULE";
export const UPDATE_SCHEDULE="UPDATE_SCHEDULE";
export const DELETE_SCHEDULE="DELETE_SCHEDULE";
export const RJ_DETAIL="RJ_DETAIL";
export const UPDATE_RJ="UPDATE_RJ";
export const UPDATE_RJ_IMAGE="UPDATE_RJ_IMAGE";
export const ALL_SCHEDULE="ALL_SCHEDULE"
export const SINGLE_SCHEDULE="SINGLE_SCHEDULE";
export const UPDATE_RJ_REQUEST="UPDATE_RJ_REQUEST";
export const SHOW_REQUEST="SHOW_REQUEST";
export const SINGLE_REQUEST="SINGLE_REQUEST";
export const SUBSCRIBER_COUNTER="SUBSCRIBER_COUNTER";
export const SUBSCRIBER_DELETE="SUBSCRIBER_DELETE";












"""radion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.contrib.auth import views as auth_views
from django.conf import settings 
from django.conf.urls.static import static 
from radio_app.views import (home,register,stationsignup,schedulesview,aboutview,schedule_edit,update_schedule,login_view)
urlpatterns = [
    path('logout/',auth_views.LogoutView.as_view(template_name='radio_app/logout.html'),name='logout'),
    path('admin/', admin.site.urls),
    path('home/',home,name='home'),
    path('signup/',register,name='signup'),
    path('signin/',login_view,name='login'),
    path('schedule/',schedulesview,name='article'),
    path('about/',aboutview,name='about'),
    path('account/api/',include('radio_app.api.urls')),
    # path('login/',auth_views.LoginView.as_view(templates=))
    path('station_signup/',stationsignup,name='stationsignup'),
    path('schedule_edit/',schedule_edit,name='schedule_edit'),
    path('update_schedule/<int:id>',update_schedule,name='update_schedule'),

    # path('own/',own_schedule_edit,name='own_schedule_edit'),
]
if settings.DEBUG: 
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT) 
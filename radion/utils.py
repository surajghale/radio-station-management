from radio_app.api.serializer import UserSerializer
def my_jwt_response_handler(token, user=None, request=None):
    return {
        
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data
    }